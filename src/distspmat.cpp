// Included by distspmat.h
#include <stdarg.h>
#include <sys/stat.h>
#include "mmio.h"

#ifndef GRAPH_GENERATOR_SEQ
#define GRAPH_GENERATOR_SEQ
#endif

#include "graph500-1.2/generator/graph_generator.h"
#include "graph500-1.2/generator/utils.h"

namespace spdm3 {

//
// Constructors & Destructor.
//
template <class IT, class VT>
DistSpMat<IT, VT>::DistSpMat(Comm *comm, spdm3_format format)
  : comm_(comm),
    mat_(format),
    global_rows_(0),
    global_cols_(0),
    row_counts_(NULL),
    row_displs_(NULL),
    col_counts_(NULL),
    col_displs_(NULL),
    nnz_counts_(NULL),
    nnz_displs_(NULL),
    distance_shifted_(NULL),
    owner_rank_(NULL),
    dist_(DIST_UNIFORM),
    name_(NULL) {
  switch (sizeof(IT)) {
    case 4: mpi_it_ = MPI_INT; break;
    case 8: mpi_it_ = MPI_LONG_LONG; break;
    default:
      printf("DistSpMat::ERROR! Wrong mpi_it_ data type.\n");
      exit(1);
  }
  switch (sizeof(LT)) {
    case 4: mpi_lt_ = MPI_INT; break;
    case 8: mpi_lt_ = MPI_LONG_LONG; break;
    default:
      printf("DistSpMat::ERROR! Wrong mpi_lt_ data type.\n");
      exit(1);
  }
  switch (sizeof(VT)) {
    case 4: mpi_vt_ = MPI_FLOAT; break;
    case 8: mpi_vt_ = MPI_DOUBLE; break;
    default:
      printf("DistSpMat::ERROR! Wrong mpi_vt_ data type.\n");
      exit(1);
  }
  AllocateAux();
}

template <class IT, class VT>
DistSpMat<IT, VT>::~DistSpMat() {
  // TODO: free the counts and displs
  DeallocateAux();
}

//
// Allocation.
//
template <class IT, class VT>
void DistSpMat<IT, VT>::AllocateAux() {
  DeallocateAux();
  row_counts_ = new IT[comm_[LAYER_COL].size];
  col_counts_ = new IT[comm_[LAYER_ROW].size];
  row_displs_ = new IT[comm_[LAYER_COL].size + 1];
  col_displs_ = new IT[comm_[LAYER_ROW].size + 1];

  nnz_counts_       = new LT[comm_[LAYER].size];
  nnz_displs_       = new LT[comm_[LAYER].size + 1];

  distance_shifted_ = new IT[NUM_COMMS];
  owner_rank_       = new IT[NUM_COMMS];
  fill_vec<IT>(distance_shifted_, NUM_COMMS, 0);
  for (int i = 0; i < NUM_COMMS; ++i)
    owner_rank_[i] = comm_[i].rank;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::DeallocateAux() {
  FREE_IF_NOT_NULL(row_counts_);
  FREE_IF_NOT_NULL(row_displs_);
  FREE_IF_NOT_NULL(col_counts_);
  FREE_IF_NOT_NULL(col_displs_);
  FREE_IF_NOT_NULL(nnz_counts_);
  FREE_IF_NOT_NULL(nnz_displs_);
  FREE_IF_NOT_NULL(distance_shifted_);
  FREE_IF_NOT_NULL(owner_rank_);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::GreedyPartitioning(IT *nnz_in_col) {
  // Supports only 1D layout for now.
  assert(layout() == ONED_BLOCK_ROW || layout() == ONED_BLOCK_COLUMN);

  int n, p;
  LT global_nnz, ideal_nnz;
  IT *counts, *displs;
  IT *max_count;
  if (layout() == ONED_BLOCK_ROW) {
    p = comm_[LAYER_COL].size;
    n = global_rows_;
    counts = row_counts_;
    displs = row_displs_;
    gen_counts_displs_balanced(global_cols_, comm_[LAYER_ROW].size,
                               col_counts_, col_displs_);
    max_local_cols_ = global_cols_;
    max_count = &max_local_rows_;
  } else {
    p = comm_[LAYER_ROW].size;
    n = global_cols_;
    counts = col_counts_;
    displs = col_displs_;
    gen_counts_displs_balanced(global_rows_, comm_[LAYER_COL].size,
                               row_counts_, row_displs_);
    max_local_rows_ = global_rows_;
    max_count = &max_local_cols_;
  }

  displs[0] = 0;
  if (rank() == 0) {
    MPI_Reduce(MPI_IN_PLACE, nnz_in_col, n,
               mpi_it_, MPI_SUM, 0, comm_[LAYER].comm);
    global_nnz = sum(nnz_in_col, n);
    ideal_nnz = global_nnz / p;
    int i, id;
    LT counter = 0;
    for (i = 0, id = 0; i < n && id < p; ++id) {
      counter = nnz_in_col[i++];
      while (counter + nnz_in_col[i] < ideal_nnz && i <= n - p + id)
        counter += nnz_in_col[i++];
      if (i <= n - p + id && i < n &&
          counter + nnz_in_col[i] - ideal_nnz < ideal_nnz - counter) {
        counter += nnz_in_col[i++];
      }
      // Last proc takes the rest.
      while (id == p - 1 && i < n)
        counter += nnz_in_col[i++];
      displs[id + 1] = i;
      nnz_counts_[id] = counter;
    }
  } else {
    MPI_Reduce(nnz_in_col, NULL, n,
               mpi_it_, MPI_SUM, 0, comm_[LAYER].comm);
  }

  MPI_Bcast(displs + 1, p, mpi_it_, 0, comm_[LAYER].comm);
  MPI_Bcast(nnz_counts_, p, mpi_lt_, 0, comm_[LAYER].comm);

  nnz_displs_[0] = 0;
  for (int id = 0; id < p; ++id) {
    counts[id] = displs[id+1] - displs[id];
    nnz_displs_[id+1] = nnz_displs_[id] + nnz_counts_[id];
    if (counts[id] > *max_count)
      *max_count = counts[id];
    if (nnz_counts_[id] > max_local_nnz_)
      max_local_nnz_ = nnz_counts_[id];
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::UniformPartitioning() {
  IT team_rows = comm_[LAYER_COL].size;
  IT team_cols = comm_[LAYER_ROW].size;
  gen_counts_displs_balanced(global_rows_, team_rows, row_counts_, row_displs_);
  gen_counts_displs_balanced(global_cols_, team_cols, col_counts_, col_displs_);

  max_local_rows_ = row_counts_[0];
  max_local_cols_ = col_counts_[0];
}

template <class IT, class VT>
void DistSpMat<IT, VT>::UpdateNnzCountAndDispls() {
  MPI_Allgather(&(mat_.nnz_), 1, mpi_lt_,
                nnz_counts_, 1, mpi_lt_, comm_[LAYER].comm);
  gen_displs_from_counts(comm_[LAYER].size, nnz_counts_, nnz_displs_);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::GetPartitioningFromLocalMatrices() {
  IT team_rows = comm_[LAYER_COL].size;
  IT team_cols = comm_[LAYER_ROW].size;
  MPI_Allgather(&(mat_.rows_), 1, mpi_it_,
                row_counts_, 1, mpi_it_, comm_[LAYER_COL].comm);
  MPI_Allgather(&(mat_.cols_), 1, mpi_it_,
                col_counts_, 1, mpi_it_, comm_[LAYER_ROW].comm);
  gen_displs_from_counts(comm_[LAYER_COL].size, row_counts_, row_displs_);
  gen_displs_from_counts(comm_[LAYER_ROW].size, col_counts_, col_displs_);
  global_rows_ = row_displs_[comm_[LAYER_COL].size];
  global_cols_ = col_displs_[comm_[LAYER_ROW].size];

  // Finds max local row and column sizes.
  max_local_rows_ = row_counts_[0];
  for (int i = 1; i < comm_[LAYER_COL].size; ++i)
    if (row_counts_[i] > max_local_rows_) max_local_rows_ = row_counts_[i];
  max_local_cols_ = col_counts_[0];
  for (int i = 1; i < comm_[LAYER_ROW].size; ++i)
    if (col_counts_[i] > max_local_cols_) max_local_cols_ = col_counts_[i];
  
  UpdateNnzCountAndDispls();
}

//
// I/O.
//
template <class IT, class VT>
void DistSpMat<IT, VT>::SetSize(int global_rows, int global_cols) {
  global_rows_ = global_rows;
  global_cols_ = global_cols;
  UniformPartitioning();
}

template <class IT, class VT>
void DistSpMat<IT, VT>::GenerateUniform(int global_rows, int global_cols,
                                        double nnz_ratio, int seed) {
  // Sets size.
  global_rows_ = global_rows;
  global_cols_ = global_cols;

  // We don't want two columns to be the same (0 and 1 are the same seed).
  if (seed == 0) seed = 1;
  UniformPartitioning();

  // Housekeeping.
  LT nnz = 0;
  int maxval = 30;
  int d = nnz_ratio * rows();  // d nnz per col.
  int min_row_displs = row_displs_[comm_[LAYER_ROW].id];
  int max_row_displs = row_displs_[comm_[LAYER_ROW].id + 1];
  int min_col_displs = col_displs_[comm_[LAYER_COL].id];
  int max_col_displs = col_displs_[comm_[LAYER_COL].id + 1];
  IT local_rows = row_counts_[comm_[LAYER_ROW].id];
  IT local_cols = col_counts_[comm_[LAYER_COL].id];

  // Sets up row vectors.
  std::vector<std::pair<IT, VT>> *rowvec =
      new std::vector<std::pair<IT, VT>>[local_rows];
  for (int i = 0; i < local_rows; ++i)
    rowvec[i].clear();

  // Goes column-by-column.
  for (int j = min_col_displs; j < max_col_displs; ++j) {
    srand(seed + j);
    for (int i = 0; i < max_row_displs; ++i) {
      // To always generate the same matrix regardless of
      // #procs or layout.
      if (i < min_row_displs) {
        rand(); rand();
        continue;
      }
      if (rand() % rows() < d) {
        int val = 1 + (rand() % maxval);
        rowvec[i-min_row_displs].push_back(std::make_pair(j-min_col_displs, val));
        ++nnz;
      } else {
        rand();
      }
    }
  }
//  print("rows, cols, nnz: %d %d %d\n", local_rows, local_cols, nnz);

  // Put them in normal SpMat.
  IT idx = 0, i;

  mat_.Allocate(local_rows, local_cols, nnz);
  for (i = 0; i < local_rows; ++i) {
    mat_.headptrs_[i] = idx;
    for (int k = 0; k < rowvec[i].size(); ++k) {
      mat_.indices_[idx] = rowvec[i][k].first;
      mat_.values_[idx]  = rowvec[i][k].second;
      ++idx;
    }
  }
  mat_.headptrs_[i] = idx;
  assert(idx == nnz);
  mat_.nnz_ = nnz;

  // Deallocation.
  delete [] rowvec;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::GenerateUniform2(int global_rows, int global_cols,
                                         double nnz_ratio, int seed) {
  // Sets size.
  global_rows_ = global_rows;
  global_cols_ = global_cols;

  // We don't want two columns to be the same (0 and 1 are the same seed).
  if (seed == 0) seed = 1;
  UniformPartitioning();
  assert(layout() == ONED_BLOCK_ROW);

  // Housekeeping.
  LT nnz = 0;
  int maxval = 30;
  int d = nnz_ratio * rows();  // d nnz per col.
  int min_row_displs = row_displs_[comm_[LAYER_ROW].id];
  int max_row_displs = row_displs_[comm_[LAYER_ROW].id + 1];
  int min_col_displs = col_displs_[comm_[LAYER_COL].id];
  int max_col_displs = col_displs_[comm_[LAYER_COL].id + 1];
  IT local_rows = row_counts_[comm_[LAYER_ROW].id];
  IT local_cols = col_counts_[comm_[LAYER_COL].id];

  // Sets up row vectors.
  std::vector<std::pair<IT, VT>> *rowvec =
      new std::vector<std::pair<IT, VT>>[local_rows];
  for (int i = 0; i < local_rows; ++i)
    rowvec[i].clear();

  // Goes row-by-row
  for (int i = min_row_displs; i < max_row_displs; ++i) {
    srand(seed + i);
    for (int k = 0; k < d; ++k) {
      int j = rand() % rows();
      int val = 1 + (rand() % maxval);
      rowvec[i-min_row_displs].push_back(std::make_pair(j, val));
      ++nnz;
    }
  }
//  print("rows, cols, nnz: %d %d %d\n", local_rows, local_cols, nnz);

  // Put them in normal SpMat.
  IT idx = 0, i;

  mat_.Allocate(local_rows, local_cols, nnz);
  for (i = 0; i < local_rows; ++i) {
    mat_.headptrs_[i] = idx;
    for (int k = 0; k < rowvec[i].size(); ++k) {
      mat_.indices_[idx] = rowvec[i][k].first;
      mat_.values_[idx]  = rowvec[i][k].second;
      ++idx;
    }
  }
  mat_.headptrs_[i] = idx;
  assert(idx == nnz);
  mat_.nnz_ = nnz;

  // Deallocation.
  delete [] rowvec;
}


/**
 * Note that GenGraph500Data will return global vertex numbers (from 1... N). The ith edge can be
 * accessed with edges[2*i] and edges[2*i+1]. There will be duplicates and the data won't be sorted.
 * Generates an edge list consisting of an RMAT matrix suitable for the Graph500 benchmark.
*/
template <class IT, class VT>
void DistSpMat<IT,VT>::GenerateWithG500(double initiator[4], int log_numverts, double nnz_ratio, int graphs)
{
	int64_t global_nodes = ((int64_t) 1) << log_numverts;
	int64_t global_edges = global_nodes * static_cast<int64_t>(global_nodes * nnz_ratio);

  global_rows_ = global_nodes;
  global_cols_ = global_nodes;
  int64_t local_edges = std::max((int) (global_edges/graphs), 1);
  int local_graphs = find_count_frontloaded(graphs, nprocs(), rank());
  int graph_offset = find_offset_frontloaded(graphs, nprocs(), rank());

  int64_t *edges = new int64_t[local_edges * 2];
  Triplet **triplets = new Triplet*[local_graphs];
  for (int g = 0; g < local_graphs; ++g) {
    triplets[g] = new Triplet[local_edges];

    // The generations use different seeds on different processors, generating independent
    // local RMAT matrices all having vertex ranges [0,...,globalmax-1]
    // Spread the two 64-bit numbers into five nonzero values in the correct range
    uint_fast32_t g500_seed[5];
    uint64_t seed1 = g + graph_offset;  // Graph id as the first seed.
    uint64_t seed2 = 2;                 // For deterministic runs.
    make_mrg_seed(seed1, seed2, g500_seed);

    // Clears the source vertex by setting it to -1.
    for (IT i = 0; i < local_edges; ++i)
      edges[2*i+0] = -1;

    generate_kronecker(0, 1, g500_seed, log_numverts, local_edges, initiator, edges);

    // Applies permutation and transfers edges to spdm3's Triplet structure.
    std::vector<uint64_t> permutation(global_nodes, 0);
    my_iota(permutation.begin(), permutation.end(), 0);  // Fills 0 to rows()-1.
    std::random_shuffle(permutation.begin(), permutation.end());
    for (int i = 0; i < local_edges; ++i) {
      triplets[g][i].row = permutation[edges[2 * i]];
      triplets[g][i].col = permutation[edges[2 * i + 1]];
      triplets[g][i].weight = 1.0;
    }
  }
  delete [] edges;

  // Fills nonzeroes.
  FillFromGraphs(triplets, local_graphs, local_edges);

  // Deallocates triplets.
  for (int g = 0; g < local_graphs; ++g)
    delete [] triplets[g];
  delete [] triplets;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::FillFromGraphs(Triplet **graphs,
                                     int local_graphs, LT local_edges) {
  // Creates new data type for triplets.
  // TODO(penpornk): Change this if I change the data type in the Triplet struct.
  MPI_Datatype TRIPLET;
  MPI_Type_contiguous(2, MPI_DOUBLE, &TRIPLET);
  MPI_Type_commit(&TRIPLET);

  // Bins triplets to processors.
  // First pass: count.
  printr("Binning triplets: first pass. %lf\n", read_timer());
  IT blk_row, blk_col;
  IT *triplet_idx = new IT[nprocs()];
  IT *triplet_counts = new IT[nprocs()];
  IT *triplet_displs = new IT[nprocs() + 1];
  IT num_local_triplets = local_edges * local_graphs;
  Triplet *triplets = new Triplet[num_local_triplets];

  fill_vec(triplet_idx, nprocs(), 0);
  fill_vec(triplet_counts, nprocs(), 0);

  if (dist_ == DIST_UNIFORM) {
    printr("Calling uniform partitioning.\n");
    UniformPartitioning();
    for (int i = 0; i < local_graphs; ++i) {
      for (int j = 0; j < local_edges; ++j) {
        ++triplet_counts[FindOwner(UNIFORM, graphs[i][j].row, graphs[i][j].col,
                                   blk_row, blk_col)];
      }
    }
    gen_displs_from_counts(nprocs(), triplet_counts, triplet_displs);
    printr("Binning triplets: second pass. %lf\n", read_timer());
    for (int i = 0; i < local_graphs; ++i) {
      for (int j = 0; j < local_edges; ++j) {
        IT owner = FindOwner(UNIFORM, graphs[i][j].row, graphs[i][j].col,
                             blk_row, blk_col);
        IT idx = triplet_displs[owner] + triplet_idx[owner];
        assert(idx < local_graphs * local_edges);
        triplets[idx].row = graphs[i][j].row - row_displs_[blk_row];
        triplets[idx].col = graphs[i][j].col - col_displs_[blk_col];
        triplets[idx].weight = graphs[i][j].weight;
        ++triplet_idx[owner];
      }
    }
  } else if (layout() == ONED_BLOCK_ROW || layout() == ONED_BLOCK_COLUMN) {
    IT lines = (layout() == ONED_BLOCK_ROW)
               ? rows()
               : cols();
    IT *nnz_in_line = new IT[lines];
    fill_vec(nnz_in_line, lines, 0);
    for (int i = 0; i < local_graphs; ++i) {
      for (int j = 0; j < local_edges; ++j) {
        if (layout() == ONED_BLOCK_ROW)
          ++nnz_in_line[graphs[i][j].row];
        else if (layout() == ONED_BLOCK_COLUMN)
          ++nnz_in_line[graphs[i][j].col];
      }
    }
    GreedyPartitioning(nnz_in_line);
    for (int i = 0; i < local_graphs; ++i) {
      for (int j = 0; j < local_edges; ++j)
        ++triplet_counts[FindOwner(BINARY_SEARCH,
                                   graphs[i][j].row, graphs[i][j].col,
                                   blk_row, blk_col)];
    }
    delete [] nnz_in_line;
    gen_displs_from_counts(nprocs(), triplet_counts, triplet_displs);
    printr("Binning triplets: second pass %lf\n", read_timer());
    for (int i = 0; i < local_graphs; ++i) {
      for (int j = 0; j < local_edges; ++j) {
        IT owner = FindOwner(BINARY_SEARCH,
                             graphs[i][j].row, graphs[i][j].col, blk_row, blk_col);
        IT idx = triplet_displs[owner] + triplet_idx[owner];
        assert(idx < num_local_triplets);
        if (layout() == ONED_BLOCK_ROW) {
          triplets[idx].row = graphs[i][j].row - row_displs_[owner];
          triplets[idx].col = graphs[i][j].col;
        } else if (layout() == ONED_BLOCK_COLUMN) {
          triplets[idx].row = graphs[i][j].row;
          triplets[idx].col = graphs[i][j].col - col_displs_[owner];
        }
        triplets[idx].weight = graphs[i][j].weight;
        ++triplet_idx[owner];
      }
    }
  } else {
    printr("Error! Trying to use Greedy partitioning with non-1D layout.\n");
    exit(1);
  }

  IT rows = row_counts_[comm_[LAYER_ROW].id];
  IT cols = col_counts_[comm_[LAYER_COL].id];

  // Exchanging triplets.
  printr("Alltoall the counts. %lf\n", read_timer());
  IT max_local_nnz;
  IT *multi_triplet_counts = new IT[nprocs()];
  IT *multi_triplet_displs = new IT[nprocs() + 1];
  MPI_Alltoall(triplet_counts, 1, mpi_it_,
               multi_triplet_counts, 1, mpi_it_, comm_[WORLD].comm);

  gen_displs_from_counts(nprocs(), multi_triplet_counts, multi_triplet_displs);
  IT my_triplets_count = multi_triplet_displs[nprocs()];
  if (layout() != ONED_BLOCK_ROW && layout() != ONED_BLOCK_COLUMN) {
    MPI_Allreduce(&my_triplets_count, &max_local_nnz, 1,
                  mpi_it_, MPI_MAX, comm_[WORLD].comm);
  }
  Triplet *multi_triplets = new Triplet[my_triplets_count];
  MPI_Alltoallv(triplets, triplet_counts, triplet_displs, TRIPLET,
                multi_triplets, multi_triplet_counts, multi_triplet_displs,
                TRIPLET, comm_[WORLD].comm);

  delete [] triplet_idx;
  delete [] triplet_counts;
  delete [] triplet_displs;

  // Converts to corresponding format.
  printr("Filling triplets. %lf\n", read_timer());
  mat_.Allocate(row_counts_[comm_[LAYER_ROW].id],
                col_counts_[comm_[LAYER_COL].id], my_triplets_count);
  mat_.Fill(multi_triplets, my_triplets_count);
  MPI_Allgather(&(mat_.nnz_), 1, mpi_lt_,
                nnz_counts_, 1, mpi_lt_, comm_[LAYER].comm);
  gen_displs_from_counts(comm_[LAYER].size, nnz_counts_, nnz_displs_);
  printr("Done FillFromGraphs. %lf\n", read_timer());

  delete [] triplets;
  delete [] multi_triplets;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::FillTriplets(std::vector<Triplet> input) {
  // Creates new data type for triplets.
  // TODO(penpornk): Change this if I change the data type in the Triplet struct.
  MPI_Datatype TRIPLET;
  MPI_Type_contiguous(2, MPI_DOUBLE, &TRIPLET);
  MPI_Type_commit(&TRIPLET);

  // Bins triplets to processors.
  // First pass: count.
  printr("Binning triplets: first pass. %lf\n", read_timer());
  IT blk_row, blk_col;
  IT *triplet_idx = new IT[teams()];
  IT *triplet_counts = new IT[teams()];
  IT *triplet_displs = new IT[teams() + 1];
  IT num_local_triplets = input.size();
  Triplet *triplets = new Triplet[num_local_triplets];

  fill_vec(triplet_idx, teams(), 0);
  fill_vec(triplet_counts, teams(), 0);

  if (dist_ == DIST_UNIFORM) {
    printr("Calling uniform partitioning.\n");
    UniformPartitioning();
    for (auto &i : input) {
      ++triplet_counts[FindOwner(UNIFORM, i.row, i.col,
                                 blk_row, blk_col)];

    }

    gen_displs_from_counts(teams(), triplet_counts, triplet_displs);
    printr("Binning triplets: second pass. %lf\n", read_timer());
    for (auto &i : input) {
      IT owner = FindOwner(UNIFORM, i.row, i.col,
                           blk_row, blk_col);
      IT idx = triplet_displs[owner] + triplet_idx[owner];
      assert(idx < num_local_triplets);
      triplets[idx].row = i.row - row_displs_[blk_row];
      triplets[idx].col = i.col - col_displs_[blk_col];
      triplets[idx].weight = i.weight;
      ++triplet_idx[owner];
    }
  } else if (layout() == ONED_BLOCK_ROW || layout() == ONED_BLOCK_COLUMN) {
    IT lines = (layout() == ONED_BLOCK_ROW)
               ? rows()
               : cols();
    IT *nnz_in_line = new IT[lines];
    fill_vec(nnz_in_line, lines, 0);
    if (layout() == ONED_BLOCK_ROW) {
      for (auto &i : input)
        ++nnz_in_line[i.row];
    } else if (layout() == ONED_BLOCK_COLUMN) {
      for (auto &i : input)
        ++nnz_in_line[i.col];
    }

    GreedyPartitioning(nnz_in_line);
    for (auto &i : input) {
      ++triplet_counts[FindOwner(BINARY_SEARCH,
                                 i.row, i.col, blk_row, blk_col)];
    }
    delete [] nnz_in_line;
    gen_displs_from_counts(teams(), triplet_counts, triplet_displs);
    printr("Binning triplets: second pass %lf\n", read_timer());
    for (auto &i : input) {
      IT owner = FindOwner(BINARY_SEARCH,
                           i.row, i.col, blk_row, blk_col);
      IT idx = triplet_displs[owner] + triplet_idx[owner];
      assert(idx < num_local_triplets);
      if (layout() == ONED_BLOCK_ROW) {
        triplets[idx].row = i.row - row_displs_[owner];
        triplets[idx].col = i.col;
      } else if (layout() == ONED_BLOCK_COLUMN) {
        triplets[idx].row = i.row;
        triplets[idx].col = i.col - col_displs_[owner];
      }
      triplets[idx].weight = i.weight;
      ++triplet_idx[owner];
    }
  } else {
    printr("Error! Trying to use Greedy partitioning with non-1D layout.\n");
    exit(1);
  }

  IT rows = row_counts_[comm_[LAYER_ROW].id];
  IT cols = col_counts_[comm_[LAYER_COL].id];

  // Exchanging triplets.
  printr("Alltoall the counts. %lf\n", read_timer());
  IT max_local_nnz;
  IT *multi_triplet_counts = new IT[teams()];
  IT *multi_triplet_displs = new IT[teams() + 1];
  MPI_Alltoall(triplet_counts, 1, mpi_it_,
               multi_triplet_counts, 1, mpi_it_, comm_[LAYER].comm);

  gen_displs_from_counts(teams(), multi_triplet_counts, multi_triplet_displs);
  IT my_triplets_count = multi_triplet_displs[teams()];
  if (layout() != ONED_BLOCK_ROW && layout() != ONED_BLOCK_COLUMN) {
    MPI_Allreduce(&my_triplets_count, &max_local_nnz, 1,
                  mpi_it_, MPI_MAX, comm_[LAYER].comm);
  }
  Triplet *multi_triplets = new Triplet[my_triplets_count];
  MPI_Alltoallv(triplets, triplet_counts, triplet_displs, TRIPLET,
                multi_triplets, multi_triplet_counts, multi_triplet_displs,
                TRIPLET, comm_[LAYER].comm);

  delete [] triplet_idx;
  delete [] triplet_counts;
  delete [] triplet_displs;
  delete [] multi_triplet_counts;
  delete [] multi_triplet_displs;

  if (layers() > 1) {
    IT *team_triplet_counts = new IT[layers()];
    IT *team_triplet_displs = new IT[layers() + 1];
    MPI_Allgather(&my_triplets_count, 1, mpi_it_,
                  team_triplet_counts, 1, mpi_it_, comm_[TEAM].comm);
    gen_displs_from_counts(layers(), team_triplet_counts, team_triplet_displs);

    IT my_team_triplets = team_triplet_displs[layers()];
    Triplet *team_triplets = new Triplet[my_team_triplets];
    MPI_Allgatherv(multi_triplets, my_triplets_count, TRIPLET,
                   team_triplets, team_triplet_counts, team_triplet_displs, TRIPLET, comm_[TEAM].comm);

    std::swap(my_triplets_count, my_team_triplets);
    std::swap(multi_triplets, team_triplets);

    delete [] team_triplets;
    delete [] team_triplet_counts;
    delete [] team_triplet_displs;
  }

  // Converts to corresponding format.
  printr("Filling triplets. %lf\n", read_timer());
  mat_.Allocate(row_counts_[comm_[LAYER_ROW].id],
                col_counts_[comm_[LAYER_COL].id], my_triplets_count);
  mat_.Fill(multi_triplets, my_triplets_count);
  MPI_Allgather(&(mat_.nnz_), 1, mpi_lt_,
                nnz_counts_, 1, mpi_lt_, comm_[LAYER].comm);
  gen_displs_from_counts(comm_[LAYER].size, nnz_counts_, nnz_displs_);
  printr("Done FillTriplets. %lf\n", read_timer());

  delete [] triplets;
  delete [] multi_triplets;
}


template <class IT, class VT>
void DistSpMat<IT, VT>::ResetCounters() {
  fill_vec<IT>(distance_shifted_, NUM_COMMS, 0);
  for (int i = 0; i < NUM_COMMS; ++i)
    owner_rank_[i] = comm_[i].rank;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::ResetShift(const spdm3_comm &comm) {
  Shift(comm, -distance_shifted_[comm]);
  assert(distance_shifted_[comm] == 0);
  assert(owner_rank_[comm] == comm_[comm].rank);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::ResetShifts() {
  ResetShift(LAYER);
  ResetShift(LAYER_ROW);
  ResetShift(LAYER_COL);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::GenerateSymmetric(int global_rows,
                                          int global_cols,
                                          double nnz_ratio,
                                          int graphs) {
  // Sets size;
  global_rows_ = global_rows;
  global_cols_ = global_cols;

  // Generates graphs.
  printr("Generating graphs. %lf\n", read_timer());
  double edges = (double) rows() * (double) cols() * nnz_ratio;
  int local_edges = std::max((int) (edges/graphs), 1);
  int local_graphs = find_count_frontloaded(graphs, nprocs(), rank());
  int graph_offset = find_offset_frontloaded(graphs, nprocs(), rank());

  int max_val = 30;
  Triplet **triplets = new Triplet*[local_graphs];
  int total_edge_count = 0;

  // Makes local_edge multiple of two.
  if (local_edges % 2 == 1) ++local_edges;
  for (int i = 0; i < local_graphs; ++i) {
    // Seeds rand().
    srand(graph_offset + i);

    // Allocates triplets.
    triplets[i] = new Triplet[local_edges];

    // Generates edges.
    for (int edge = 0; edge < local_edges; edge += 2) {
      int row_id = rand() % global_rows_;
      int col_id = rand() % global_cols_;
      int weight = (rand() % max_val) - (max_val >> 1);
      if (weight <= 0) --weight;

      triplets[i][edge].row    = row_id;
      triplets[i][edge].col    = col_id;
      triplets[i][edge].weight = weight;

      triplets[i][edge+1].row    = col_id;
      triplets[i][edge+1].col    = row_id;
      triplets[i][edge+1].weight = weight;
    }

    total_edge_count += local_edges;
  }

  FillFromGraphs(triplets, local_graphs, local_edges);

  // Deallocates graphs.
  for (int i = 0; i < local_graphs; ++i)
    delete [] triplets[i];
  delete [] triplets;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SetIdentity(int n) {
  // Sets size.
  global_rows_ = n;
  global_cols_ = n;
  UniformPartitioning();

  IT dim1_count, dim2_count;
  IT min_dim1, max_dim1, min_dim2, max_dim2;
  if (mat_.format_ == SPARSE_CSR) {
    dim1_count = row_counts_[comm_[LAYER_ROW].id];
    dim2_count = col_counts_[comm_[LAYER_COL].id];
    min_dim1 = row_displs_[comm_[LAYER_ROW].id];
    max_dim1 = row_displs_[comm_[LAYER_ROW].id + 1];
    min_dim2 = col_displs_[comm_[LAYER_COL].id];
    max_dim2 = col_displs_[comm_[LAYER_COL].id + 1];
  } else {
    dim1_count = col_counts_[comm_[LAYER_COL].id];
    dim2_count = row_counts_[comm_[LAYER_ROW].id];
    min_dim1 = col_displs_[comm_[LAYER_COL].id];
    max_dim1 = col_displs_[comm_[LAYER_COL].id + 1];
    min_dim2 = row_displs_[comm_[LAYER_ROW].id];
    max_dim2 = row_displs_[comm_[LAYER_ROW].id + 1];
  }

  IT max_min = std::max(min_dim1, min_dim2);
  IT min_max = std::min(max_dim1, max_dim2);
  mat_.nnz_ = min_max - max_min;
  mat_.Allocate(dim1_count, dim2_count, mat_.nnz_);

  IT k = 0;
  IT li1 = 0;
  IT li2 = max_min - min_dim2;
  mat_.headptrs_[li1] = k;
  for (IT i1 = min_dim1; i1 < max_dim1; ++i1) {
    if (max_min <= i1 && i1 < min_max) {
      mat_.values_[k] = 1.0;
      mat_.indices_[k++] = li2++;
    }
    mat_.headptrs_[++li1] = k;
  }
}

inline void check_newline(int *bytes_read, int bytes_requested, char *buf)
{
    if ((*bytes_read) < bytes_requested) {
        // fewer bytes than expected, this means EOF
        if (buf[(*bytes_read) - 1] != '\n') {
            // doesn't terminate with a newline, add one to prevent infinite loop later
            buf[(*bytes_read) - 1] = '\n';
            printf("Error in Matrix Market format, appending missing newline at end of file\n")		;
            (*bytes_read)++;
        }
    }
}

#define NOFILE          3004
#define MAXLINELENGTH   200
#define ONEMILLION      1000000

inline bool FetchBatch(MPI_File & infile, MPI_Offset & curpos, MPI_Offset end_fpos, bool firstcall, std::vector<std::string> & lines, int myrank)
{
    size_t bytes2fetch = ONEMILLION;    // we might read more than needed but no problem as we won't process them
    char * buf = new char[bytes2fetch];
    char * originalbuf = buf;   // so that we can delete it later because "buf" will move
    MPI_Status status;
    int bytes_read;
    if(firstcall)
    {
        curpos -= 1;    // first byte is to check whether we started at the beginning of a line
        bytes2fetch += 1;
    }

    MPI_File_read_at(infile, curpos, buf, bytes2fetch, MPI_CHAR, &status);
    MPI_Get_count(&status, MPI_CHAR, &bytes_read);  // MPI_Get_Count can only return 32-bit integers
    if(!bytes_read)
    {
        delete [] originalbuf;
        return true;    // done
    }
    check_newline(&bytes_read, bytes2fetch, buf);
    if(firstcall)
    {
        if(buf[0] == '\n')  // we got super lucky and hit the line break
        {
            buf += 1;
            bytes_read -= 1;
            curpos += 1;
        }
        else    // skip to the next line and let the preceeding processor take care of this partial line
        {
            char *c = (char*)memchr(buf, '\n', MAXLINELENGTH); //  return a pointer to the matching byte or NULL if the character does not occur
            if (c == NULL) {
                printf("Unexpected line without a break\n");
            }
            int n = c - buf + 1;
            bytes_read -= n;
            buf += n;
            curpos += n;
        }
    }
    while(bytes_read > 0 && curpos < end_fpos)  // this will also finish the last line
    {
        char *c = (char*)memchr(buf, '\n', bytes_read); //  return a pointer to the matching byte or NULL if the character does not occur
        if (c == NULL) {
            delete [] originalbuf;
            return false;  // if bytes_read stops in the middle of a line, that line will be re-read next time since curpos has not been moved forward yet
        }
        int n = c - buf + 1;

        // string constructor from char * buffer: copies the first n characters from the array of characters pointed by s
        lines.push_back(std::string(buf, n-1));  // no need to copy the newline character
        bytes_read -= n;   // reduce remaining bytes
        buf += n;   // move forward the buffer
        curpos += n;
    }
    delete [] originalbuf;
    if (curpos >= end_fpos) return true;  // don't call it again, nothing left to read
    else    return false;
}

static void push_to_vector(std::vector<Triplet> &triplets, int64_t ii, int64_t jj, double vv, int symmetric)
{
    --ii;  /* adjust from 1-based to 0-based */
    --jj;
    triplets.push_back((Triplet) {(int) ii, (int) jj, vv});
    if (symmetric && ii != jj) {
      triplets.push_back((Triplet) {(int) jj, (int) ii, vv});
    }
}

static void ProcessLines(std::vector<Triplet> &triplets, std::vector<std::string> & lines, int symmetric, int type)
{
    std::vector<std::string>::iterator itr;
    if(type == 0)   // real
    {
        int64_t ii, jj;
        double vv;
        for (itr=lines.begin(); itr != lines.end(); ++itr)
        {
            // string::c_str() -> Returns a pointer to an array that contains a null-terminated sequence of characters (i.e., a C-string)
            sscanf(itr->c_str(), "%" PRId64 " %" PRId64 " %lg", &ii, &jj, &vv);
            push_to_vector(triplets, ii, jj, vv, symmetric);
        }
    }
    else if(type == 1) // integer
    {
        int64_t ii, jj, vv;
        for (itr=lines.begin(); itr != lines.end(); ++itr)
        {
            sscanf(itr->c_str(), "%" PRId64 " %" PRId64 " %" PRId64, &ii, &jj, &vv);
            push_to_vector(triplets, ii, jj, (double) vv, symmetric);
        }
    }
    else if(type == 2) // pattern
    {
        int64_t ii, jj;
        for (itr=lines.begin(); itr != lines.end(); ++itr)
        {
            sscanf(itr->c_str(), "%" PRId64 " %" PRId64, &ii, &jj);
            push_to_vector(triplets, ii, jj, 1.0, symmetric);
        }
    }
    else
    {
        printf("COMBBLAS: Unrecognized matrix market scalar type\n");
    }
    std::vector<std::string>().swap(lines);
}

// Adapted from Aydin's COMBBLAS.
// Handles all sorts of orderings as long as there are no duplicates.
// Requires proper matrix market banner at the moment.
template <class IT, class VT>
void DistSpMat<IT, VT>::LoadMatrixMarket(const char *filename)
{
  int32_t type = -1;
  int32_t symmetric = 0;
  int64_t nrows, ncols, nonzeros;
  int64_t linesread = 0;

  FILE *f;
  int myrank = rank();
  int numprocs = nprocs();
  if(myrank == 0) {
    MM_typecode matcode;
    if ((f = fopen(filename, "r")) == NULL) {
      printf("COMBBLAS: Matrix-market file can not be found\n");
      MPI_Abort(MPI_COMM_WORLD, NOFILE);
    }
    if (mm_read_banner(f, &matcode) != 0) {
      printf("Could not process Matrix Market banner.\n");
      exit(1);
    }
    linesread++;

    if (mm_is_complex(matcode)) {
      printf("Sorry, this application does not support complext types");
      printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
    } else if(mm_is_real(matcode)) {
      printf("Matrix is Float\n");
      type = 0;
    } else if(mm_is_integer(matcode)) {
      printf("Matrix is Integer\n");
      type = 1;
    } else if(mm_is_pattern(matcode)) {
      printf("Matrix is Boolean\n");
      type = 2;
    }
    if(mm_is_symmetric(matcode)) {
      printf("Matrix is symmetric\n");
      symmetric = 1;
    }

    int ret_code;
    // ABAB: mm_read_mtx_crd_size made 64-bit friendly
    if ((ret_code = mm_read_mtx_crd_size(f, &nrows, &ncols, &nonzeros, &linesread)) !=0)
      exit(1);

    printf("Total number of entries expected across all processors is %" PRId64 "\n", nonzeros);
  }
  MPI_Bcast(&type, 1, MPI_INT, 0, comm_[WORLD].comm);
  MPI_Bcast(&symmetric, 1, MPI_INT, 0, comm_[WORLD].comm);
  MPI_Bcast(&nrows, 1, MPI_LONG_LONG, 0, comm_[WORLD].comm);
  MPI_Bcast(&ncols, 1, MPI_LONG_LONG, 0, comm_[WORLD].comm);
  MPI_Bcast(&nonzeros, 1, MPI_LONG_LONG, 0, comm_[WORLD].comm);

  this->global_rows_ = nrows;
  this->global_cols_ = ncols;
  this->printr("Dimension: %d x %d\n", this->global_rows_, this->global_cols_);

  // Use fseek again to go backwards two bytes and check that byte with fgetc
  struct stat st;     // get file size
  if (stat(filename, &st) == -1) {
    MPI_Abort(MPI_COMM_WORLD, NOFILE);
  }
  int64_t file_size = st.st_size;
  MPI_Offset fpos, end_fpos, fpos_offset;
  if (myrank == 0) {   // the offset needs to be for this rank
    printf("File is %" PRId64 " bytes\n", file_size);
    fpos_offset = ftell(f);
    fclose(f);
  }
  MPI_Bcast(&fpos_offset, 1, MPI_OFFSET, 0, comm_[WORLD].comm);
  int64_t process_size = file_size - fpos_offset;
  fpos = fpos_offset + myrank * process_size / numprocs;

  if (myrank != (numprocs-1)) end_fpos = fpos_offset + (myrank + 1) * process_size / numprocs;
  else end_fpos = file_size;
  //printf("%d: fpos %d, end_fpos %d\n", this->grid_.rank, fpos, end_fpos);

  MPI_File mpi_fh;
  MPI_File_open (comm_[WORLD].comm, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &mpi_fh);

  // ABAB: It is the user's job that VT is compatible with "type" and IT is compatible with int64_t
  std::vector<Triplet> triplets;

  std::vector<std::string> lines;
  bool finished = FetchBatch(mpi_fh, fpos, end_fpos, true, lines, myrank);
  int64_t entriesread = lines.size();
  ProcessLines(triplets, lines, symmetric, type);
  MPI_Barrier(comm_[WORLD].comm);

  while (!finished) {
    finished = FetchBatch(mpi_fh, fpos, end_fpos, false, lines, myrank);
    entriesread += lines.size();
    ProcessLines(triplets, lines, symmetric, type);
  }
  int64_t allentriesread;
  MPI_Reduce(&entriesread, &allentriesread, 1, MPI_LONG_LONG, MPI_SUM, 0, comm_[WORLD].comm);

  this->printr("Total number of entries read across all processors is %lld\n", allentriesread);
//  print("num local triplets %d\n", triplets.size());
  FillTriplets(triplets);

  // Deallocation.
  triplets.clear();
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SaveMatrixMarket(const char *filename) {
  // Only layer 0 needs to participate.
  if (layer_id() > 0)
    return;

  // Each process writes to separate files.
  char name[100];
  sprintf(name, "%s-%05d", filename, rank());
  FILE *f_out = fopen(name, "w");

  // Only rank 0 needs to write a header.
  LT global_nnz = nnz();
  if (rank() == 0) {
    fprintf(f_out, "%%%%MatrixMarket matrix coordinate real general\n");
    fprintf(f_out, "%d %d %d\n", global_rows_, global_cols_, global_nnz);
  }

  // Writes each nonzero as a triplet.
  // 1-based index.
  if (mat_.format_ == SPARSE_CSR) {
    for (IT i = 0; i < mat_.rows_; ++i) {
      for (LT k = mat_.headptrs_[i]; k < mat_.headptrs_[i+1]; ++k) {
        IT j = mat_.indices_[k];
        fprintf(f_out, "%10d %10d %30.16g\n",
            row_displs() + i + 1, col_displs() + j + 1, mat_.values_[k]);
      }
    }
  } else {
    for (IT j = 0; j < mat_.cols_; ++j) {
      for (LT k = mat_.headptrs_[j]; k < mat_.headptrs_[j+1]; ++k) {
        IT i = mat_.indices_[k];
        fprintf(f_out, "%10d %10d %30.16g\n",
            row_displs() + i + 1, col_displs() + j + 1, mat_.values_[k]);
      }
    }
  }

  // Closes the file.
  fclose(f_out);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SaveLocal(const char *filename) {
  mat_.Save(filename, rank());
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SaveLocalDense(const char *filename) {
  mat_.SaveDense(filename, rank());
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SaveTeam(const char *filename) {
  if (layer_id() == 0)
    mat_.Save(filename, lrank());
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SaveTeamDense(const char *filename) {
  if (layer_id() == 0)
    mat_.SaveDense(filename, lrank());
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Save(const char *filename) {
  if (layer_id() == 0) {
    SpMat<IT, VT> whole = GetWholeMatrix();
    whole.Save(filename);
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SaveDense(const char *filename) {
  if (layer_id() == 0) {
    SpMat<IT, VT> whole = GetWholeMatrix();
    whole.SaveDense(filename);
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SavePPM(const char *filename) {
  if (layer_id() == 0) {
    SpMat<IT, VT> whole = GetWholeMatrix();
    whole.SavePPM(filename);
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SavePNG(const char *filename) {
  if (layer_id() == 0) {
    SpMat<IT, VT> whole = GetWholeMatrix();
    whole.SavePNG(filename);
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Diagonal(const DistSpMat<IT, VT> &X) {
  // Must be square matrix.
  assert(X.rows() == X.cols());

  CommonCopy(X);
  mat_.Allocate(X.lrows(), X.lcols(), std::min(X.lrows(), X.lcols()));

  mat_.nnz_ = 0;
  mat_.headptrs_[0] = 0;
  for (IT i = 0; i < X.mat_.dim1(); ++i) {
    IT global_i = i + X.dim1_displs();
    for (LT k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      IT j = X.mat_.indices_[k];
      IT global_j = j + X.dim2_displs();
      if (global_i == global_j) {
        mat_.values_[mat_.nnz_] = X.mat_.values_[k];
        mat_.indices_[mat_.nnz_] = j;
        ++mat_.nnz_;
      } else if (global_i < global_j) {
        continue;
      }
    }
    mat_.headptrs_[i+1] = mat_.nnz_;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Diagonal(const DistDMat<IT, VT> &X) {
  // Must be square matrix.
  assert(X.rows() == X.cols());

  CommonCopy(X);
  IT dim1_count, dim2_count;
  IT min_dim1, max_dim1, min_dim2, max_dim2;
  if (mat_.format_ == SPARSE_CSR) {
    dim1_count = row_counts_[comm_[LAYER_ROW].id];
    dim2_count = col_counts_[comm_[LAYER_COL].id];
    min_dim1 = row_displs_[comm_[LAYER_ROW].id];
    max_dim1 = row_displs_[comm_[LAYER_ROW].id + 1];
    min_dim2 = col_displs_[comm_[LAYER_COL].id];
    max_dim2 = col_displs_[comm_[LAYER_COL].id + 1];
  } else {
    dim1_count = col_counts_[comm_[LAYER_COL].id];
    dim2_count = row_counts_[comm_[LAYER_ROW].id];
    min_dim1 = col_displs_[comm_[LAYER_COL].id];
    max_dim1 = col_displs_[comm_[LAYER_COL].id + 1];
    min_dim2 = row_displs_[comm_[LAYER_ROW].id];
    max_dim2 = row_displs_[comm_[LAYER_ROW].id + 1];
  }

  IT max_min = std::max(min_dim1, min_dim2);
  IT min_max = std::min(max_dim1, max_dim2);
  mat_.nnz_ = min_max - max_min;
  mat_.Allocate(dim1_count, dim2_count, mat_.nnz_);

  IT k = 0;
  IT li1 = 0;
  IT li2 = max_min - min_dim2;
  mat_.headptrs_[li1] = k;
  for (IT i1 = min_dim1; i1 < max_dim1; ++i1) {
    if (max_min <= i1 && i1 < min_max) {
      if ((mat_.format_ == SPARSE_CSR && X.mat_.format_ == DENSE_ROWMAJOR) ||
          (mat_.format_ == SPARSE_CSC && X.mat_.format_ == DENSE_COLMAJOR)) {
        mat_.values_[k] = X.mat_.values_[li1 * X.mat_.lda_ + li2];
      } else {
        mat_.values_[k] = X.mat_.values_[li1 + X.mat_.lda_ * li2];
      }
      mat_.indices_[k++] = li2++;
    }
    mat_.headptrs_[++li1] = k;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Convert(const DistDMat<IT, VT> &X) {
  CommonCopy(X);
  mat_.Convert(X.mat_);
  // TODO(penpornk): Update max_local_nnz_.
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Add(const VT alpha, const DistSpMat<IT, VT> &A,
                            const VT beta, const DistSpMat<IT, VT> &B) {
  // TODO(penpornk): Check if A and B have the same layout.
  CommonCopy(A);
  mat_.Add(alpha, A.mat_, beta, B.mat_);
  // TODO(penpornk): Update max_local_nnz_.
}

template <class IT, class VT>
void DistSpMat<IT, VT>::WeirdLayoutBlockColumn(const DistSpMat<IT, VT> &X) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_COLUMN);

  // Finds new number of rows and nnz.
  int nrows = 0, nnz = 0;
  int c = layers();
  int nteams = teams();
  int nblocks = nteams / c;
  int blk_idx = (team_id() + layer_id()) % nteams;
  for (int b = 0; b < nblocks; ++b) {
    int num_rows = col_counts_[blk_idx];
    int global_row_displs = col_displs_[blk_idx];
    for (int i = global_row_displs; i < global_row_displs + num_rows; ++i) {
      int nnz_in_row = X.mat_.headptrs_[i+1] - X.mat_.headptrs_[i];
      nnz += nnz_in_row;
    }
    nrows += num_rows;
    blk_idx = (blk_idx + c) % nteams;
  }

  // Allocation.
  mat_.Allocate(nrows, X.lcols(), nnz);

  // Goes block by block and set submatrix
  int blk_row_offset = 0;
  mat_.nnz_ = 0;
  mat_.headptrs_[0] = 0;
  blk_idx = (team_id() + layer_id()) % nteams;
  for (int b = 0; b < nblocks; ++b) {
    int global_row_displs = col_displs_[blk_idx];
    int num_rows = col_displs_[blk_idx + 1] - col_displs_[blk_idx];

    // Copies row-by-row.
    for (int i = 0; i < num_rows; ++i) {
      int global_row_id = global_row_displs + i;
      int nnz_in_row = X.mat_.headptrs_[global_row_id+1] - X.mat_.headptrs_[global_row_id];

      // TODO(penpornk): update indices when we don't take the whole block column.
      memcpy(mat_.indices_ + mat_.nnz_,
             X.mat_.indices_ + X.mat_.headptrs_[global_row_id], nnz_in_row * sizeof(IT));
      memcpy(mat_.values_ + mat_.nnz_,
             X.mat_.values_ + X.mat_.headptrs_[global_row_id], nnz_in_row * sizeof(VT));
      mat_.headptrs_[blk_row_offset + i] = mat_.nnz_;
      mat_.nnz_ += nnz_in_row;
    }
    mat_.headptrs_[blk_row_offset + num_rows] = mat_.nnz_;
    blk_row_offset += num_rows;

    blk_idx = (blk_idx + c) % nteams;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::WeirdLayoutBlockRow(const DistSpMat<IT, VT> &X) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_ROW);

  // Finds new number of cols and nnz.
  int ncols = 0, nnz = 0;
  int nrows = X.lrows();
  int c = layers();
  int nteams = teams();
  int nblocks = nteams / c;
  int tid = team_id();
  int lid = layer_id();

  // Finds #cols and creates displs array.
  int *displs = new int[nblocks + 1];
  int blk_idx = (tid + c - (lid % c)) % c;
  displs[0] = 0;
  for (int b = 0; b < nblocks; ++b) {
    ncols += row_counts_[blk_idx];
    displs[b+1] = ncols;
    blk_idx += c;
  }

  // Finds #nnz.
  for (int i = 0; i < nrows; ++i) {
    int start_blk_idx = 0;
    for (int k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      // Searches what block column idx the column index belongs to.
      // In the next turn starts searching from where we left off.
      for (blk_idx = start_blk_idx;
           blk_idx < nteams && X.mat_.indices_[k] >= row_displs_[blk_idx+1]; ++blk_idx);
      start_blk_idx = blk_idx;

      int layer_idx = (tid + c - (blk_idx % c)) % c;
      if (layer_idx == lid)
        ++nnz;
    }
  }

  // Allocation.
  mat_.Allocate(nrows, ncols, nnz);

  // Sets submatrix.
  mat_.nnz_ = 0;
  mat_.headptrs_[0] = 0;
  for (int i = 0; i < nrows; ++i) {
    int start_blk_idx = 0;
    for (int k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      // Searches what block column idx the column index belongs to.
      // In the next turn starts searching from where we left off.
      for (blk_idx = start_blk_idx;
           blk_idx < nteams && X.mat_.indices_[k] >= row_displs_[blk_idx+1]; ++blk_idx);
      start_blk_idx = blk_idx;

      int layer_idx = (tid + c - (blk_idx % c)) % c;
      int col_offset = displs[blk_idx / c];
      if (layer_idx == lid) {
        mat_.indices_[mat_.nnz_] = X.mat_.indices_[k] - row_displs_[blk_idx] + col_offset;
        mat_.values_[mat_.nnz_] = X.mat_.values_[k];
        ++mat_.nnz_;
      }
    }
    mat_.headptrs_[i+1] = mat_.nnz_;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::WeirdLayoutBlockRowTransposed(const DistSpMat<IT, VT> &X) {
  CommonCopy(X);
  assert(rows() == cols());  // Must be square.
  assert(layout() == ONED_BLOCK_ROW);

  // Finds new number of cols and nnz.
  int ncols = 0, nnz = 0;
  int nrows = X.lrows();
  int c = layers();
  int nteams = teams();
  int nblocks = nteams / c;
  int tid = team_id();
  int lid = layer_id();

  // Finds #cols and creates displs array.
  int *displs = new int[nblocks + 1];
  int blk_idx = (tid + lid) % c;
  displs[0] = 0;
  for (int b = 0; b < nblocks; ++b) {
    ncols += row_counts_[blk_idx];
    displs[b+1] = ncols;
    blk_idx += c;
  }

  // Finds #nnz.
  for (int i = 0; i < nrows; ++i) {
    int start_blk_idx = 0;
    for (int k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      // Searches what block column idx the column index belongs to.
      // In the next turn starts searching from where we left off.
      for (blk_idx = start_blk_idx;
           blk_idx < nteams && X.mat_.indices_[k] >= row_displs_[blk_idx+1]; ++blk_idx);
      start_blk_idx = blk_idx;

      int layer_idx = (blk_idx + c - (tid % c)) % c;
      if (layer_idx == lid)
        ++nnz;
    }
  }

  // Allocation.
  mat_.Allocate(nrows, ncols, nnz);

  // Sets submatrix.
  mat_.nnz_ = 0;
  mat_.headptrs_[0] = 0;
  for (int i = 0; i < nrows; ++i) {
    int start_blk_idx = 0;
    for (int k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      // Searches what block column idx the column index belongs to.
      // In the next turn starts searching from where we left off.
      for (blk_idx = start_blk_idx;
           blk_idx < nteams && X.mat_.indices_[k] >= row_displs_[blk_idx+1]; ++blk_idx);
      start_blk_idx = blk_idx;

      int layer_idx = (blk_idx + c - (tid % c)) % c;
      int col_offset = displs[blk_idx / c];
      if (layer_idx == lid) {
        mat_.indices_[mat_.nnz_] = X.mat_.indices_[k] - row_displs_[blk_idx] + col_offset;
        mat_.values_[mat_.nnz_] = X.mat_.values_[k];
        ++mat_.nnz_;
      }
    }
    mat_.headptrs_[i+1] = mat_.nnz_;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::WeirdLayoutDiagonal(const DistSpMat<IT, VT> &X) {
  // Must be square matrix.
  assert(X.rows() == X.cols());
  assert(layout() == ONED_BLOCK_ROW);  // Supports only 1D block row for now.

  CommonCopy(X);
  if (layer_id() > 0) {
    mat_.Allocate(X.lrows(), X.lcols(), 0);
    mat_.nnz_ = 0;
    for (IT i = 0; i <= X.mat_.dim1(); ++i)
      mat_.headptrs_[i] = 0;
    return;
  }

  // Allocation.
  mat_.Allocate(X.lrows(), X.lcols(), std::min(X.lrows(), X.lcols()));

  // Creates displs array.
  int nlayers = layers();
  int nteams = teams();
  int nblocks = nteams / nlayers;
  int tid = team_id();
  int blk_idx = (tid + nlayers - (layer_id() % nlayers)) % nlayers;
  int *displs = new int[nblocks + 1];
  displs[0] = 0;
  for (int i = 0; i < nblocks; ++i) {
    displs[i+1] = displs[i] + row_counts_[blk_idx];
    blk_idx += nlayers;
  }

  // Forms the matrix.
  int global_dim1_offset = X.dim1_displs();
  blk_idx = tid / nlayers;
  mat_.nnz_ = 0;
  mat_.headptrs_[0] = 0;
  for (IT i = 0; i < X.mat_.dim1(); ++i) {
    IT b, starting_b = 0;
    for (LT k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      IT j = X.mat_.indices_[k];
      for (b = starting_b; b < nblocks && j >= displs[b+1]; ++b);
      starting_b = b;

      if (b < blk_idx) continue;
      else if (b > blk_idx) break;

      IT jj = j - displs[blk_idx];
      if (i == jj) {
        mat_.values_[mat_.nnz_] = X.mat_.values_[k];
        mat_.indices_[mat_.nnz_] = j;
        ++mat_.nnz_;
      } else if (i < jj) {
        continue;
      }
    }
    mat_.headptrs_[i+1] = mat_.nnz_;
  }

  delete [] displs;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::WeirdLayoutDiagonalTransposed(const DistSpMat<IT, VT> &X) {
  // Must be square matrix.
  assert(X.rows() == X.cols());
  assert(layout() == ONED_BLOCK_ROW);  // Supports only 1D block row for now.

  CommonCopy(X);
  if (layer_id() > 0) {
    mat_.Allocate(X.lrows(), X.lcols(), 0);
    mat_.nnz_ = 0;
    for (IT i = 0; i <= X.mat_.dim1(); ++i)
      mat_.headptrs_[i] = 0;
    return;
  }

  // Allocation.
  mat_.Allocate(X.lrows(), X.lcols(), std::min(X.lrows(), X.lcols()));

  // Creates displs array.
  int nlayers = layers();
  int nteams = teams();
  int nblocks = nteams / nlayers;
  int tid = team_id();
  int blk_idx = (tid + layer_id()) % nlayers;
  int *displs = new int[nblocks + 1];
  displs[0] = 0;
  for (int i = 0; i < nblocks; ++i) {
    displs[i+1] = displs[i] + row_counts_[blk_idx];
    blk_idx += nlayers;
  }

  // Forms the matrix.
  int global_dim1_offset = X.dim1_displs();
  blk_idx = tid / nlayers;
  mat_.nnz_ = 0;
  mat_.headptrs_[0] = 0;
  for (IT i = 0; i < X.mat_.dim1(); ++i) {
    IT b, starting_b = 0;
    for (LT k = X.mat_.headptrs_[i]; k < X.mat_.headptrs_[i+1]; ++k) {
      IT j = X.mat_.indices_[k];
      for (b = starting_b; b < nblocks && j >= displs[b+1]; ++b);
      starting_b = b;

      if (b < blk_idx) continue;
      else if (b > blk_idx) break;

      IT jj = j - displs[blk_idx];
      if (i == jj) {
        mat_.values_[mat_.nnz_] = X.mat_.values_[k];
        mat_.indices_[mat_.nnz_] = j;
        ++mat_.nnz_;
      } else if (i < jj) {
        continue;
      }
    }
    mat_.headptrs_[i+1] = mat_.nnz_;
  }

  delete [] displs;
}


template <class IT, class VT>
void DistSpMat<IT, VT>::print(const char *fmt, ...) {
  char *content;
  IT len = strlen(fmt);
  content = new char [len + 20];

  if (name_ == NULL)
    sprintf(content, "%d: %s", rank(), fmt);
  else
    sprintf(content, "%d: (%s) %s\n", rank(), name_, fmt);

  va_list args;
  va_start(args, fmt);
  vprintf(content, args);
  va_end(args);

  delete [] content;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::printr(const char *fmt, ...) {
  if (rank() != 0) return;
  char *content;
  IT len = strlen(fmt);
  content = new char [len + 20];

  if (name_ == NULL)
    sprintf(content, "%d: %s", rank(), fmt);
  else
    sprintf(content, "%d: (%s) %s\n", rank(), name_, fmt);

  va_list args;
  va_start(args, fmt);
  vprintf(content, args);
  va_end(args);

  delete [] content;
}

//
// Operations.
//
template <class IT, class VT>
void DistSpMat<IT, VT>::CheckCommsAreInitialized() const {
  if (comm_ == NULL) {
    printf("DistDMat::rank() ERROR! Communicators aren't initialized yet.\n");
    exit(1);
  }
}

template <class IT, class VT>
bool DistSpMat<IT, VT>::CheckDimMatch(const DistDMat<IT, VT> &X) {
  // TODO(penpornk): implement.
}

template <class IT, class VT>
bool DistSpMat<IT, VT>::CheckDimMatch(const DistSpMat<IT, VT> &X) {
  // TODO(penpornk): implement.
}

template <class IT, class VT>
void DistSpMat<IT, VT>::SwapBuffers() {
  // To be used by Shift. Shift will handle rows_ and cols_.
  std::swap(mat_.nnz_, tmp_mat_.nnz_);

  std::swap(mat_.head_size_, tmp_mat_.head_size_);
  std::swap(mat_.buffer_size_, tmp_mat_.buffer_size_);
  std::swap(mat_.min_head_size_, tmp_mat_.min_head_size_);
  std::swap(mat_.min_buffer_size_, tmp_mat_.min_buffer_size_);

  std::swap(mat_.free_buffers_, tmp_mat_.free_buffers_);
  std::swap(mat_.headptrs_, tmp_mat_.headptrs_);
  std::swap(mat_.indices_, tmp_mat_.indices_);
  std::swap(mat_.values_, tmp_mat_.values_);
}

template <class IT, class VT>
VT DistSpMat<IT, VT>::FrobeniusNorm() {
  VT fnorm_sqr = mat_.FrobeniusNormSquare();
  MPI_Allreduce(MPI_IN_PLACE, &fnorm_sqr, 1, mpi_vt_, MPI_SUM, comm_[LAYER].comm);
  return sqrt(fnorm_sqr);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::AddPiggyback(const VT *values, const IT len) {
  mat_.ExpandBuffers(mat_.nnz_ + len, true);
  for (int i = 0; i < len; ++i)
    mat_.values_[mat_.nnz_ + i] = values[i];
}

template <class IT, class VT>
const VT *DistSpMat<IT, VT>::GetPiggyback() {
  return mat_.values_ + mat_.nnz_;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::ElmtWiseOp(std::function<VT(VT)> fn) {
  mat_.ElmtWiseOp(fn);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::AddI(VT alpha, VT beta, const DistSpMat<IT, VT> &X) {
  CheckDimMatch(X);
  // TODO(penpornk): Use DMat ElmtWiseOpt routine.
}

template <class IT, class VT>
void DistSpMat<IT, VT>::DotMultiplyI(const DistSpMat<IT, VT> &X) {
  CheckDimMatch(X);
  // It seems this isn't used.
  // TODO(penpornk): Use SpMat ElmtWiseOpt routine.
}

template <class IT, class VT>
void DistSpMat<IT, VT>::DotMultiplyI(const DistDMat<IT, VT> &X) {
  CheckDimMatch(X);
  mat_.DotMultiplyI(X.mat_);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::DotMultiplyTransposeI(const DistDMat<IT, VT> &X) {
  mat_.DotMultiplyTransposeI(X.mat_);
}

template <class IT, class VT>
DistSpMat<IT, VT> DistSpMat<IT, VT>::Add(VT alpha, VT beta,
                                         const DistSpMat<IT, VT> &X) {
}

template <class IT, class VT>
DistSpMat<IT, VT> DistSpMat<IT, VT>::GetShallowCopy(bool transfer_buffer_ownership) {
  DistSpMat<IT, VT> S(comm_);
  S.global_rows_ = global_rows_;
  S.global_cols_ = global_cols_;

  memcpy(S.row_counts_, row_counts_, comm_[LAYER_COL].size * sizeof(IT));
  memcpy(S.col_counts_, col_counts_, comm_[LAYER_ROW].size * sizeof(IT));
  memcpy(S.nnz_counts_, nnz_counts_, comm_[LAYER].size * sizeof(LT));

  memcpy(S.row_displs_, row_displs_, (comm_[LAYER_COL].size+1) * sizeof(IT));
  memcpy(S.col_displs_, col_displs_, (comm_[LAYER_ROW].size+1) * sizeof(IT));
  memcpy(S.nnz_displs_, nnz_displs_, (comm_[LAYER].size+1) * sizeof(LT));

  S.mat_ = mat_.GetShallowCopy(transfer_buffer_ownership);
  return S;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::CommonCopy(const DistSpMat<IT, VT> &X) {
  global_rows_ = X.global_rows_;
  global_cols_ = X.global_cols_;

  max_local_rows_ = X.max_local_rows_;
  max_local_cols_ = X.max_local_cols_;
  max_local_nnz_  = X.max_local_nnz_;

  memcpy(row_counts_, X.row_counts_, comm_[LAYER_COL].size * sizeof(IT));
  memcpy(col_counts_, X.col_counts_, comm_[LAYER_ROW].size * sizeof(IT));
  memcpy(nnz_counts_, X.nnz_counts_, comm_[LAYER].size * sizeof(LT));

  memcpy(row_displs_, X.row_displs_, (comm_[LAYER_COL].size+1) * sizeof(IT));
  memcpy(col_displs_, X.col_displs_, (comm_[LAYER_ROW].size+1) * sizeof(IT));
  memcpy(nnz_displs_, X.nnz_displs_, (comm_[LAYER].size+1) * sizeof(LT));

  memcpy(distance_shifted_, X.distance_shifted_, NUM_COMMS * sizeof(IT));
  memcpy(owner_rank_, X.owner_rank_, NUM_COMMS * sizeof(IT));

  name_ = X.name_;
  mpi_it_ = X.mpi_it_;
  mpi_lt_ = X.mpi_lt_;
  mpi_vt_ = X.mpi_vt_;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::CommonCopy(const DistDMat<IT, VT> &X) {
  global_rows_ = X.global_rows_;
  global_cols_ = X.global_cols_;

  max_local_rows_ = X.max_local_rows_;
  max_local_cols_ = X.max_local_cols_;
  max_local_nnz_  = -1;

  memcpy(row_counts_, X.row_counts_, comm_[LAYER_COL].size * sizeof(IT));
  memcpy(col_counts_, X.col_counts_, comm_[LAYER_ROW].size * sizeof(IT));

  memcpy(row_displs_, X.row_displs_, (comm_[LAYER_COL].size+1) * sizeof(IT));
  memcpy(col_displs_, X.col_displs_, (comm_[LAYER_ROW].size+1) * sizeof(IT));

  memcpy(distance_shifted_, X.distance_shifted_, NUM_COMMS * sizeof(IT));
  memcpy(owner_rank_, X.owner_rank_, NUM_COMMS * sizeof(IT));

  name_ = X.name_;
  mpi_it_ = X.mpi_it_;
  mpi_lt_ = X.mpi_lt_;
  mpi_vt_ = X.mpi_vt_;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::DeepCopy(const DistSpMat<IT, VT> &X) {
  // comm_ and layout() must be set since in the constructor.
  CommonCopy(X);
  mat_.DeepCopy(X.mat_);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::ShallowCopy(const DistSpMat<IT, VT> &X) {
  CommonCopy(X);
  mat_.ShallowCopy(X.mat_);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::TakeOwnership(DistSpMat<IT, VT> &X) {
  mat_.TakeOwnership(X.mat_);
}

template <class IT, class VT>
DistSpMat<IT, VT> DistSpMat<IT, VT>::GetDeepCopy() {
  DistSpMat<IT, VT> S(comm_);
  S.global_rows_ = global_rows_;
  S.global_cols_ = global_cols_;

  memcpy(S.row_counts_, row_counts_, comm_[LAYER_COL].size * sizeof(IT));
  memcpy(S.col_counts_, col_counts_, comm_[LAYER_ROW].size * sizeof(IT));
  memcpy(S.nnz_counts_, nnz_counts_, comm_[LAYER].size * sizeof(LT));

  memcpy(S.row_displs_, row_displs_, (comm_[LAYER_COL].size+1) * sizeof(IT));
  memcpy(S.col_displs_, col_displs_, (comm_[LAYER_ROW].size+1) * sizeof(IT));
  memcpy(S.nnz_displs_, nnz_displs_, (comm_[LAYER].size+1) * sizeof(LT));

  S.mat_ = mat_.GetDeepCopy();
  return S;
}

template <class IT, class VT>
DistSpMat<IT, VT> DistSpMat<IT, VT>::Diagonal() {
  DistSpMat<IT, VT> D = GetShallowCopy();
  D.mat_.Allocate(mat_.rows_, mat_.cols_, mat_.rows_);

  LT nnz = 0;
  D.mat_.headptrs_[0] = 0;
  for (IT i = 0; i < mat_.dim1(); ++i) {
    IT global_i = i + dim1_displs();
    for (LT k = mat_.headptrs_[i]; k < mat_.headptrs_[i+1]; ++k) {
      IT j = mat_.indices_[k];
      IT global_j = j + dim2_displs();
      if (global_i == global_j) {
        D.mat_.values_[nnz] = mat_.values_[k];
        D.mat_.indices_[nnz] = j;
        ++nnz;
      } else if (global_i < global_j)
        continue;
    }
    D.mat_.headptrs_[i+1] = nnz;
  }
  D.mat_.nnz_ = nnz;
  return D;
}

//
// Communication.
//
template <class IT, class VT>
void DistSpMat<IT, VT>::Shift(const spdm3_comm &comm, const IT &distance) {
  if (distance == 0)
    return;

  int src = (comm_[comm].rank + comm_[comm].size + distance) % comm_[comm].size;
  int dest = (comm_[comm].rank + comm_[comm].size - distance) % comm_[comm].size;
//  print("rank, size, dist, src, dest: %d %d %d %d %d\n",
//      rank, size, distance, src, dest);

  distance_shifted_[comm] =
      (distance_shifted_[comm] + comm_[comm].size + distance) % comm_[comm].size;
  owner_rank_[comm] =
      (owner_rank_[comm] + comm_[comm].size + distance) % comm_[comm].size;

  // Sends everything out at once.
  // Assumes max_ldim1() + 1 is the maximum length of all headptrs.
  tmp_mat_.ExpandHeads(max_ldim1() + 1);
  MPI_Request mpi_send_request[3];
  MPI_Status mpi_recv_stats;
//  print("Heads: Sending %d, Receiving %d\n", mat_.dim1() + 1, max_ldim1() + 1);
  MPI_Isend(mat_.headptrs_, mat_.dim1() + 1,
            mpi_lt_, dest, 0, comm_[comm].comm, &mpi_send_request[0]);
  MPI_Isend(mat_.indices_, mat_.nnz_,
            mpi_lt_, dest, 0, comm_[comm].comm, &mpi_send_request[1]);
  MPI_Isend(mat_.values_, mat_.nnz_,
            mpi_vt_, dest, 0, comm_[comm].comm, &mpi_send_request[2]);

  // Receives head pointers first, then counts the number of headptrs received.
  MPI_Recv(tmp_mat_.headptrs_, max_ldim1() + 1,
           mpi_lt_, src, 0, comm_[comm].comm, &mpi_recv_stats);
  IT new_heads;
  MPI_Get_count(&mpi_recv_stats, mpi_lt_, &new_heads);

  // Expands tmp_mat_'s buffers if necessary.
  tmp_mat_.nnz_ = tmp_mat_.headptrs_[new_heads-1];
//  print("nnz: sending %d, receiving %d\n", mat_.nnz_, tmp_mat_.nnz_);
  tmp_mat_.ExpandBuffers(tmp_mat_.nnz_, false);
  MPI_Recv(tmp_mat_.indices_, tmp_mat_.nnz_,
           mpi_lt_, src, 0, comm_[comm].comm, &mpi_recv_stats);
  MPI_Recv(tmp_mat_.values_, tmp_mat_.nnz_,
           mpi_vt_, src, 0, comm_[comm].comm, &mpi_recv_stats);

  // Gets the actual nnz's received for sanity check.
  int new_nnz;  // MPI_Get_count requires int.
  MPI_Get_count(&mpi_recv_stats, mpi_vt_, &new_nnz);
  assert(new_nnz == tmp_mat_.nnz_);

  // Waits for all Isends to be done then swaps buffers.
  MPI_Waitall(3, mpi_send_request, MPI_STATUSES_IGNORE);
  SwapBuffers();

  // Figures out the new #cols and #rows.
  switch (comm) {
    case LAYER_ROW:
      mat_.cols_ = col_counts_[owner_rank_[comm]];
      break;
    case LAYER_COL:
      mat_.rows_ = row_counts_[owner_rank_[comm]];
      break;
    default:
      if (layout() == ONED_BLOCK_ROW)
        mat_.rows_ = row_counts_[owner_rank_[comm]];
      else
        mat_.cols_ = col_counts_[owner_rank_[comm]];
      break;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::ShiftPiggyback(const spdm3_comm &comm, const IT &distance,
                                       const IT piggyback_len) {
  int src = (comm_[comm].rank + comm_[comm].size + distance) % comm_[comm].size;
  int dest = (comm_[comm].rank + comm_[comm].size - distance) % comm_[comm].size;
//  print("rank, size, dist, src, dest: %d %d %d %d %d\n",
//      rank, size, distance, src, dest);

  int new_length = mat_.nnz_ + piggyback_len;
  distance_shifted_[comm] =
      (distance_shifted_[comm] + comm_[comm].size + distance) % comm_[comm].size;
  owner_rank_[comm] =
      (owner_rank_[comm] + comm_[comm].size + distance) % comm_[comm].size;

  // Sends everything out at once.
  // Assumes max_ldim1() + 1 is the maximum length of all headptrs.
  tmp_mat_.ExpandHeads(max_ldim1() + 1);
  MPI_Request mpi_send_request[3];
  MPI_Status mpi_recv_stats;
//  print("Heads: Sending %d, Receiving %d\n", mat_.dim1() + 1, max_ldim1() + 1);
  MPI_Isend(mat_.headptrs_, mat_.dim1() + 1,
            mpi_lt_, dest, 0, comm_[comm].comm, &mpi_send_request[0]);
  MPI_Isend(mat_.indices_, new_length,
            mpi_lt_, dest, 0, comm_[comm].comm, &mpi_send_request[1]);
  MPI_Isend(mat_.values_, new_length,
            mpi_vt_, dest, 0, comm_[comm].comm, &mpi_send_request[2]);

  // Receives head pointers first, then counts the number of headptrs received.
  MPI_Recv(tmp_mat_.headptrs_, max_ldim1() + 1,
           mpi_lt_, src, 0, comm_[comm].comm, &mpi_recv_stats);
  IT new_heads;
  MPI_Get_count(&mpi_recv_stats, mpi_lt_, &new_heads);

  // Expands tmp_mat_'s buffers if necessary.
  tmp_mat_.nnz_ = tmp_mat_.headptrs_[new_heads-1];
  int expected_len = tmp_mat_.nnz_ + piggyback_len;
//  print("nnz: sending %d, receiving %d\n", mat_.nnz_, tmp_mat_.nnz_);
  tmp_mat_.ExpandBuffers(expected_len, false);
  MPI_Recv(tmp_mat_.indices_, expected_len,
           mpi_lt_, src, 0, comm_[comm].comm, &mpi_recv_stats);
  MPI_Recv(tmp_mat_.values_, expected_len,
           mpi_vt_, src, 0, comm_[comm].comm, &mpi_recv_stats);

  // Gets the actual nnz's received for sanity check.
  int received_items;  // MPI_Get_count requires int.
  MPI_Get_count(&mpi_recv_stats, mpi_vt_, &received_items);
  assert(received_items == expected_len);

  // Waits for all Isends to be done then swaps buffers.
  MPI_Waitall(3, mpi_send_request, MPI_STATUSES_IGNORE);
  SwapBuffers();

  // Figures out the new #cols and #rows.
  switch (comm) {
    case LAYER_ROW:
      mat_.cols_ = col_counts_[owner_rank_[comm]];
      break;
    case LAYER_COL:
      mat_.rows_ = row_counts_[owner_rank_[comm]];
      break;
    default:
      if (layout() == ONED_BLOCK_ROW)
        mat_.rows_ = row_counts_[owner_rank_[comm]];
      else
        mat_.cols_ = col_counts_[owner_rank_[comm]];
      break;
  }
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Bcast(const spdm3_comm &comm, const IT &root) {
  // Finds out the dimensions of the matrix being broadcast.
  IT heads;
  if (comm == LAYER_COL) {
    mat_.rows_ = row_counts_[root];
    mat_.cols_ = col_counts_[comm_[comm].id];
  } else if (comm == LAYER_ROW) {
    mat_.rows_ = row_counts_[comm_[comm].id];
    mat_.cols_ = col_counts_[root];
  } else {
      printf("DistSpMat::Bcast: ERROR! Layout not supported.\n");
      exit(1);
  }

  // Expands headptrs if too small.
  if (comm_[comm].rank != root)
    mat_.ExpandHeads(mat_.dim1() + 1, false);

  // Broadcasts head pointers.
  // TODO(penpornk): We can cut to the exact dim1 instead of max_.ldim1().
  MPI_Bcast(mat_.headptrs_, mat_.dim1() + 1,
            mpi_lt_, root, comm_[comm].comm);
  LT new_nnz = mat_.headptrs_[mat_.dim1()];

  // TODO(penpornk): Check buffer size.
  mat_.nnz_ = new_nnz;
  mat_.ExpandBuffers(mat_.nnz_, false);
  MPI_Bcast(mat_.indices_, mat_.nnz_, mpi_lt_, root, comm_[comm].comm);
  MPI_Bcast(mat_.values_, mat_.nnz_, mpi_vt_, root, comm_[comm].comm);
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Reduce(const spdm3_comm &comm, const IT &root) {
  // Very crappy and non-versatile implementation of Reduce.
  // But we don't need it for sparse matrix anyway..
  if (comm_[comm].rank == root) {
    MPI_Reduce(MPI_IN_PLACE, mat_.values_, mat_.nnz_,
               mpi_vt_, MPI_SUM, root, comm_[comm].comm);
  } else {
    MPI_Reduce(mat_.values_, NULL, mat_.nnz_,
               mpi_vt_, MPI_SUM, root, comm_[comm].comm);
  }
}

template <class IT, class VT>
SpMat<IT, VT> *DistSpMat<IT, VT>
    ::GetLocalSpMatsFromComm(const Comm& comm) {
  IT *row_counts = new IT[comm.size];
  IT *col_counts = new IT[comm.size];
  IT *row_displs = new IT[comm.size + 1];
  IT *col_displs = new IT[comm.size + 1];

  // Gets row and column counts.
  MPI_Allgather(&(mat_.rows_), 1, mpi_it_,
                row_counts, 1, mpi_it_, comm.comm);
  MPI_Allgather(&(mat_.cols_),  1, mpi_it_,
                col_counts, 1, mpi_it_, comm.comm);

  // Calculates row and column displacements.
  gen_displs_from_counts(comm.size, row_counts, row_displs);
  gen_displs_from_counts(comm.size, col_counts, col_displs);

  // Must be int because of Allgatherv.
  int *dim1_counts = new int[comm.size];
  int *dim1_displs = new int[comm.size + 1];

  // Prepares count and displs for Allgatherv of head pointers.
  if (mat_.format_ == SPARSE_CSR) {
    for (int i = 0; i < comm.size; ++i)
      dim1_counts[i] = row_counts[i] + 1;
  } else if (mat_.format_ == SPARSE_CSC) {
    for (int i = 0; i < comm.size; ++i)
      dim1_counts[i] = col_counts[i] + 1;
  }
  gen_displs_from_counts(comm.size, dim1_counts, dim1_displs);

  // Gathers head pointers.
  LT *multi_headptrs = new LT[dim1_displs[comm.size]];
  MPI_Allgatherv(mat_.headptrs_, mat_.dim1() + 1, mpi_lt_,
                 multi_headptrs, dim1_counts, dim1_displs, mpi_lt_,
                 comm.comm);

  // Prepares count and displs for Allgatherv of indices and values.
  int *values_counts = new int[comm.size];
  int *values_displs = new int[comm.size + 1];
  for (int i = 0; i < comm.size; ++i)
    values_counts[i] = multi_headptrs[dim1_displs[i] + dim1_counts[i] - 1];
  gen_displs_from_counts(comm.size, values_counts, values_displs);

  // Gathers indices and values.
  LT *multi_indices = new LT[values_displs[comm.size]];
  VT *multi_values  = new VT[values_displs[comm.size]];
  MPI_Allgatherv(mat_.indices_, mat_.nnz_, mpi_lt_,
                 multi_indices, values_counts, values_displs, mpi_lt_,
                 comm.comm);
  MPI_Allgatherv(mat_.values_, mat_.nnz_, mpi_vt_,
                 multi_values, values_counts, values_displs, mpi_vt_,
                 comm.comm);

  // Creates an array of SpMats.
  SpMat<IT, VT> *M = new SpMat<IT, VT>[comm.size];
  for (int i = 0; i < comm.size; ++i) {
    M[i].PointersToSpMat(row_counts[i], col_counts[i], values_counts[i],
                        mat_.format_, mat_.idx_base_,
                        multi_headptrs + dim1_displs[i],
                        multi_indices + values_displs[i],
                        multi_values + values_displs[i], (i == 0));
  }

  // Deallocation.
  delete [] row_counts;
  delete [] col_counts;
  delete [] row_displs;
  delete [] col_displs;
  delete [] dim1_counts;
  delete [] dim1_displs;
  delete [] values_counts;
  delete [] values_displs;

  return M;
}

template <class IT, class VT>
SpMat<IT, VT> DistSpMat<IT, VT>::GetWholeMatrix() {
  ResetShifts();
  SpMat<IT, VT> *M = GetLocalSpMatsFromComm(comm_[LAYER]);
  SpMat<IT, VT> mat(M, comm_[LAYER_COL].size, comm_[LAYER_ROW].size, layout());
  return mat;
}

template <class IT, class VT>
void DistSpMat<IT, VT>::Replicate(Comm* new_comm) {
  SpMat<IT, VT> *M = GetLocalSpMatsFromComm(new_comm[TEAM]);
  mat_.Concatenate(M, new_comm[TEAM_COL].size, new_comm[TEAM_ROW].size, new_comm[WORLD].layout);

  // Updates counts and displs.
  int nteams = new_comm[LAYER].size;
  int team_rows = new_comm[LAYER_COL].size;
  int team_cols = new_comm[LAYER_ROW].size;
  int c = new_comm[TEAM].size;
  int crows = comm_[LAYER_COL].size / team_rows;
  int ccols = comm_[LAYER_ROW].size / team_cols;

  // Row counts.
  for (int i = 0; i < team_rows; ++i) {
    int ic = i * crows;
    row_counts_[i] = row_counts_[ic];
    row_displs_[i] = row_displs_[ic];
    for (int j = 1; j < crows; ++j)
      row_counts_[i] += row_counts_[ic + j];

    // Updates max local rows.
    if (row_counts_[i] > max_local_rows_)
      max_local_rows_ = row_counts_[i];
  }
  row_displs_[team_rows] = row_displs_[comm_[LAYER_COL].size];

  // Col counts.
  for (int i = 0; i < team_cols; ++i) {
    int ic = i * ccols;
    col_counts_[i] = col_counts_[ic];
    col_displs_[i] = col_displs_[ic];
    for (int j = 1; j < ccols; ++j)
      col_counts_[i] += col_counts_[ic + j];

    // Updates max local cols.
    if (col_counts_[i] > max_local_cols_)
      max_local_cols_ = col_counts_[i];
  }
  col_displs_[team_cols] = col_displs_[comm_[LAYER_ROW].size];

  // Nnz counts.
  for (int i = 0; i < nteams; ++i) {
    int ic = i * c;
    nnz_counts_[i] = nnz_counts_[ic];
    nnz_displs_[i] = nnz_displs_[ic];
    for (int j = 1; j < c; ++j)
      nnz_counts_[i] += nnz_counts_[ic +j];

    // Updates max local nnz.
    if (nnz_counts_[i] > max_local_nnz_)
      max_local_nnz_ = nnz_counts_[i];
  }
  nnz_displs_[nteams] = col_displs_[comm_[LAYER].size];

  // Updates communicators.
  comm_ = new_comm;

  // Reset counters.
  fill_vec<IT>(distance_shifted_, NUM_COMMS, 0);
  for (int i = 0; i < NUM_COMMS; ++i)
    owner_rank_[i] = comm_[i].rank;
}

//
// Getters.
//
template <class IT, class VT>
IT DistSpMat<IT, VT>::rank() const {
  CheckCommsAreInitialized();
  return comm_[WORLD].rank;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::nprocs() const {
  CheckCommsAreInitialized();
  return comm_[WORLD].size;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::rows() const {
  return global_rows_;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::cols() const {
  return global_cols_;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::row_displs() const {
  return row_displs_[comm_[LAYER_ROW].id];
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::col_displs() const {
  return col_displs_[comm_[LAYER_COL].id];
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::dim1() const {
  switch (mat_.format_) {
    case SPARSE_CSR: return global_rows_;
    case SPARSE_CSC: return global_cols_;
    default:         return global_rows_;
  }
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::dim2() const {
  switch (mat_.format_) {
    case SPARSE_CSR: return global_cols_;
    case SPARSE_CSC: return global_rows_;
    default:         return global_cols_;
  }
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::dim1_displs() const {
  CheckCommsAreInitialized();
  switch (mat_.format_) {
    case SPARSE_CSR: return row_displs_[comm_[LAYER_COL].rank];
    case SPARSE_CSC: return col_displs_[comm_[LAYER_ROW].rank];
    default:         return row_displs_[comm_[LAYER_COL].rank];
  }
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::dim2_displs() const {
  CheckCommsAreInitialized();
  switch (mat_.format_) {
    case SPARSE_CSR: return col_displs_[comm_[LAYER_ROW].rank];
    case SPARSE_CSC: return row_displs_[comm_[LAYER_COL].rank];
    default:         return col_displs_[comm_[LAYER_ROW].rank];
  }
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::lrows() const {
  return mat_.rows_;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::lcols() const {
  return mat_.cols_;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::max_ldim1() const {
  switch (mat_.format_) {
    case SPARSE_CSR: return max_local_rows_;
    case SPARSE_CSC: return max_local_cols_;
    default:         return max_local_rows_;
  }
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::max_ldim2() const {
  switch (mat_.format_) {
    case SPARSE_CSR: return max_local_cols_;
    case SPARSE_CSC: return max_local_rows_;
    default:         return max_local_cols_;
  }
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::lrank() const {
  CheckCommsAreInitialized();
  return comm_[LAYER].rank;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::trank() const {
  CheckCommsAreInitialized();
  return comm_[TEAM].rank;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::layer_id() const {
  CheckCommsAreInitialized();
  return comm_[LAYER].id;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::team_id() const {
  CheckCommsAreInitialized();
  return comm_[TEAM].id;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::layers() const {
  return comm_[TEAM].size;
}

template <class IT, class VT>
IT DistSpMat<IT, VT>::teams() const {
  return comm_[LAYER].size;
}

template <class IT, class VT>
spdm3_layout DistSpMat<IT, VT>::layout() const {
  return comm_[WORLD].layout;
}

// Using MPI_Reduce for now: only rank 0 gets the real #nnz.
template <class IT, class VT>
LT DistSpMat<IT, VT>::nnz() const {
  LT nnz = mat_.nnz_;
  LT global_nnz = 0;
  MPI_Reduce(&nnz, &global_nnz, 1, mpi_lt_, MPI_SUM, 0, comm_[LAYER].comm);
  return global_nnz;
}

// Only rank 0 gets the real %nnz.
template <class IT, class VT>
VT DistSpMat<IT, VT>::nnz_ratio() const {
  return (VT) nnz() / (VT) rows() / (VT) cols();
}

//
// Queries.
//
template <class IT, class VT>
IT DistSpMat<IT, VT>::FindOwner(spdm3_find_owner method,
                                IT global_row_id, IT global_col_id,
                                IT &blk_row, IT &blk_col) {
  if (method == BINARY_SEARCH) {
    blk_row = find_owner_search(global_row_id, comm_[LAYER_COL].size,
                                row_displs_);
    blk_col = find_owner_search(global_col_id, comm_[LAYER_ROW].size,
                                col_displs_);
  } else if (method == UNIFORM) {
    blk_row = find_owner_uniform(global_row_id, comm_[LAYER_COL].size,
                                 row_displs_);
    blk_col = find_owner_uniform(global_col_id, comm_[LAYER_ROW].size,
                                 col_displs_);
  }
  if (layout() != TWOD_COLMAJOR)
    return blk_row * comm_[LAYER_ROW].size + blk_col;
  return blk_row + comm_[LAYER_COL].size * blk_col;
}

template <class IT, class VT>
VT DistSpMat<IT, VT>::FrobeniusNormSquare() {
  VT partial_norm_square = mat_.FrobeniusNormSquare();
  MPI_Allreduce(MPI_IN_PLACE, &partial_norm_square, 1, mpi_vt_, MPI_SUM, comm_[LAYER].comm);
  return partial_norm_square;
}

}  // namespace spdm3
