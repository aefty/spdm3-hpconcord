#include <cstdio>
#include <cmath>

#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

Timer T;
IT inner_loop = 0;
IT outer_loop = 0;

// Inputs:  Omega, S.
// Outputs: OS, h1 (scalar).
VT compute_h1(DistSpMat<IT, VT> &Omega,
              DistDMat<IT, VT> &S, DistDMat<IT, VT> &OS) {
  T.Start("Compute h1");
  DistSpMat<IT, VT> D(Omega.comm_, Omega.mat_.format_);
  D.Diagonal(Omega);
  D.ElmtWiseOp(log);
  VT partial_log_det = D.mat_.Reduce(SPDM3_SUM);
  VT partial_trace = innerABC_shiftc_w_trace<IT, VT>(Omega, S, OS);

  // Gets total det and trace.
  VT array[2] = {partial_log_det, partial_trace};
  MPI_Allreduce(MPI_IN_PLACE, array, 2, D.mpi_vt_, MPI_SUM, D.comm_[LAYER].comm);
  
  VT h1 = -array[0] + 0.5 * array[1];
  D.print("%f %f\n", array[0], array[1]);
  T.Stop("Compute h1");
  
  return h1;
}

// Inputs:  Omega, OS.
// Outputs: gh1.
void compute_gh1(DistSpMat<IT, VT> &Omega,
                 DistDMat<IT, VT> &OS, DistDMat<IT, VT> &gh1) {
  T.Start("Compute gh1");
  DistDMat<IT, VT> gh1_bcol(OS.comm_, OS.mat_.format_);
  innerABC_shiftc_transpose<IT, VT>(OS, gh1_bcol);  // gh1 = (OS)^T = SO.
  gh1_bcol.ElmtWiseOp([](VT a, VT b)->VT { return (a + b) / 2.0; }, OS);
  
  // Changes to block row layout. This only costs local transpose.
  gh1.Transpose(gh1_bcol);
  
  DistSpMat<IT, VT> d(Omega.comm_, Omega.mat_.format_);
  d.Diagonal(Omega);
  gh1.ElmtWiseOpNzOnly([](VT a, VT b)->VT { return a - (1.0 / b); }, d);
  T.Stop("Compute gh1");
}

// Inputs:  Omega_old, gh1, tau, lambda1.
// Outputs: Omega.
void compute_Omega(DistSpMat<IT, VT> &Omega_old, DistDMat<IT, VT> &gh1,
                   VT tau, DistDMat<IT, VT> &Lambda1, DistSpMat<IT, VT> &Omega) {
  T.Start("Compute Omega");
  DistDMat<IT, VT> res(Omega_old.comm_, gh1.mat_.format_);
  res.StoreElmtWise([tau](VT a, VT b)->VT { return b - tau * a; }, gh1, Omega_old);
  
  // Soft-thresholding.
  res.ElmtWiseOp([tau](VT a, VT thres)->VT {
                   VT sign = (a > 0) - (a < 0);
                   return sign * std::max(fabs(a) - (tau * thres), 0.0);
                 }, Lambda1);
  Omega.Convert(res);
  T.Stop("Compute Omega");
}

// Inputs:  Omega, Omega_old, gh1, h1_old, tau.
// Outputs: Q, maxdiff.
VT compute_Q(DistSpMat<IT, VT> &Omega, DistSpMat<IT, VT> &Omega_old,
             DistDMat<IT, VT> &gh1, VT h1_old, VT tau, VT &maxdiff) {
  T.Start("Compute Q");
  DistSpMat<IT, VT> D(Omega.comm_, Omega.mat_.format_);
  D.Add(-1.0, Omega_old, 1.0, Omega);
  maxdiff = D.mat_.Reduce(SPDM3_MAX_ABS);
  
  VT frobenius = D.FrobeniusNormSquare();
  frobenius /= 2.0 * tau;
  
  D.DotMultiplyI(gh1);
  VT trace = D.mat_.Reduce(SPDM3_SUM);
  MPI_Allreduce(MPI_IN_PLACE, &trace, 1, D.mpi_vt_, MPI_SUM, D.comm_[LAYER].comm);
  
  VT Q = h1_old + trace + frobenius;
  D.print("h1, trace, fro: %lf %lf %lf\n", h1_old, trace, frobenius);
  T.Stop("Compute Q");
  
  return Q;
}

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  
  // Parses parameters.
  char *input  = read_string(argc, argv, "-i", (char *) "xf.npy");
  char *output = read_string(argc, argv, "-o", (char *) "output.npy");
  VT lambda1 = read_double(argc, argv, "-l1", 0.3);  // 0.1
  VT lambda2 = read_double(argc, argv, "-l2", 0);  // 0.2
  VT tau0 = read_double(argc, argv, "-tau", 1.0);
  VT epsilon = read_double(argc, argv, "-eps", 1.0e-5);
  
  // Starts timer.
  T.Start("Overall");
  
  //
  // Initialization.
  //
  T.Start("Input");
  Comm *blockCol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  Comm *blockRow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  DistDMat<IT, VT> X(blockCol, DENSE_ROWMAJOR);
  DistDMat<IT, VT> XT(blockRow, DENSE_ROWMAJOR);
  XT.LoadNumPyTranspose(input);
  X.Transpose(XT);
  T.Stop("Input");
  
  T.Start("Compute S");
  DistDMat<IT, VT> S(blockCol, DENSE_ROWMAJOR);
  DistDMat<IT, VT> OS(blockCol, DENSE_ROWMAJOR);
  DistDMat<IT, VT> gh1(blockRow, DENSE_ROWMAJOR);
  XT.MultiplyAdd(X, S);
  S.ElmtWiseOp([](VT a, VT b)->VT { return a / b; }, (VT) XT.cols());
  T.Stop("Compute S");

  T.Start("Set Identity");
  DistSpMat<IT, VT> Omega(blockRow, SPARSE_CSR);
  DistSpMat<IT, VT> Omega2(blockRow, SPARSE_CSR);
  Omega.SetIdentity(S.rows());
  T.Stop("Set Identity");
  
  T.Start("Prepare Lambda1 matrix");
  // All lambda1's except with 0 diagonal elements.
  DistDMat<IT, VT> Lambda1(blockRow, DENSE_ROWMAJOR);
  Lambda1.Fill(Omega.rows(), Omega.cols(), lambda1);
  Lambda1.ElmtWiseOpNzOnly([](VT a, VT b)->VT { return 0.0; }, Omega);
  T.Stop("Prepare Lambda1 matrix");
  
  //
  // CONCORD-ISTA.
  //
  VT h1, h1_old, Q, maxdiff;
  DistSpMat<IT, VT> *Oold = &Omega;
  DistSpMat<IT, VT> *O = &Omega2;
  h1_old = compute_h1(*Oold, S, OS);
  O->printr("h1 = %10.4lf, time: %10.4lf, %%nnz: %10.7lf\n", h1_old, T.GetElapsedTime(), O->nnz_ratio() * 100.0);
  outer_loop = 0;
  compute_gh1(*Oold, OS, gh1);
  gh1.Save("sol-gh1");
  VT tau = 0.5;
  compute_Omega(*Oold, gh1, tau, Lambda1, *O);
  
  h1 = compute_h1(*O, S, OS);
  OS.SaveLocal("sol-os");
  Q = compute_Q(*O, *Oold, gh1, h1_old, tau, maxdiff);
  tau = 0.25;
  compute_Omega(*Oold, gh1, tau, Lambda1, *O);
  O->SaveDense("sol-O");

//  IT diagitr = 0;
//  while (true) {
//    compute_gh1(*Oold, OS, gh1);
//
//    diagitr = 0;
//    inner_loop = 0;
//    for (VT tau = tau0; true; tau /= 2.0) {
//      compute_Omega(*Oold, gh1, tau, Lambda1, *O);
//      DistSpMat<IT, VT> D(O->comm_);
//      D.Diagonal(*O);
//      VT min_elmt = D.mat_.Reduce(SPDM3_MIN);
//      MPI_Allreduce(MPI_IN_PLACE, &min_elmt, 1, D.mpi_vt_, MPI_MIN, D.comm_[WORLD].comm);
//      if (min_elmt < 1e-08 && diagitr < 10) {
//        ++diagitr;
//        continue;
//      }
//      
//      h1 = compute_h1(*O, S, OS);
//      Q = compute_Q(*O, *Oold, gh1, h1_old, tau, maxdiff);
//      O->printr("Round %03d.%02d [%10.4lf]:     tau = %10.4f, h1 = %10.4lf, Q = %10.4lf\n",
//          T.GetElapsedTime(), outer_loop, inner_loop, tau, h1, Q);
//      ++inner_loop;
//      
//      if (h1 <= Q || inner_loop >= 20)
//        break;
//    }
//  
//    MPI_Allreduce(MPI_IN_PLACE, &maxdiff, 1, O->mpi_vt_, MPI_MAX, MPI_COMM_WORLD);
//    ++outer_loop;
//    O->printr("time: %10.4lf, %%nnz: %10.7lf, maxdiff: %10.4lf\n",
//        T.GetElapsedTime(), O->nnz_ratio() * 100.0, maxdiff);
//    if (maxdiff < epsilon || outer_loop >= 100)
//      break;
//    
//    std::swap(h1, h1_old);
//    std::swap(O, Oold);
//    
//  }
//
//  // Save output.
//  Omega.Save(output);
//  Omega.SaveDense("Omg");  // #debug
  
  // Deallocation.
  delete [] blockCol;
  delete [] blockRow;
  
  // Finalization.
  T.Stop("Overall");
  T.Report();
  MPI_Finalize();
}