#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdlib.h>  // For exit().
#include "spdm3.h"

using namespace spdm3;

void read_params(
  int argc, char *argv[],  // Command line parameters.
  int &m, int &k, int &n,  // Matrix dimensions.
  int &cA, int &cB,        // Replication factors.
  double &percent_nnz,     // Percent nonzeroes of A.
  char *&filename_A,        // Input filename for A.
  char *&filename_B,        // Input filename for B.
  char *&filename_C         // Output filename for C.
) {
  // Parses command-line parameters.
  // The numbers at the end are the default values.
  m = read_int(argc, argv, "-m", 111);    // #rows of A and C.
  k = read_int(argc, argv, "-k", 101);    // #columns of A and #rows of B.
  n = read_int(argc, argv, "-n", 123);    // #columns of B and C.

  cA = read_int(argc, argv, "-cA", 1);    // A's replication factor.
  cB = read_int(argc, argv, "-cB", 1);    // B's replication factor.

  percent_nnz =
      read_double(argc, argv, "-pnnz", 5.0);  // Percent nonzeroes of A.
  
  // Input filenames for A and B. Output filename for C.
  // Takes priority over values of m/n/k when not null.
  filename_A = read_string(argc, argv, "-iA", NULL);
  filename_B = read_string(argc, argv, "-iB", NULL);
  filename_C = read_string(argc, argv, "-o", (char *) "C.npy");
}

void print_help(int argc, char *argv[], Timer &T) {
  // If -h, prints usage and terminates.
  if (find_option(argc, argv, "-h") > 0) {
    T.printr(
    "Usage: <launcher> -n <#processes> %s [options]\n"
    "  <launcher> is usually mpirun. Use srun on Edison/Cori supercomputer at NERSC.\n"
    "  <#processes> is the number of processes to use.\n"
    "  [options] are optional. Default values will be used if not specified.\n"
    "    -m <integer>  : #rows of A and C.\n"
    "    -k <integer>  : #cols of A (which is also #rows of B).\n"
    "    -n <integer>  : #cols of B and C.\n"
    "    -cA <integer> : #copies to replicate A (a.k.a. replication factor of A.)\n"
    "    -cB <integer> : #copies to replicate B (a.k.a. replication factor of B.)\n"
    "    -iA <string>  : input filename for A (Matrix Market format).\n"
    "    -iB <string>  : input filename for B (NumPy format).\n"
    "    -o <string>   : output filename for C. Will be in NumPy format.\n"
  
    "\n"
    ">> Restrictions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"
    "  cA * cB must not be greater than #processes.\n"
    "  Do not quote file names in quotation marks.\n"
    
    "\n"
    ">> Examples >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"
    "  export OMP_NUM_THREADS=4\n"
    "    sets the number of threads to be used per process.\n"
    "  mpirun -n 4 ./innerA -iA A.mtx -iB B.npy -o C.npy -cA 2\n"
    "    runs on 4 processes, reads A from A.mtx, replicates 2 copies of A,\n"
    "    reads B from B.npy, and saves C to C.npy.\n"
    "  mpirun -n 8 ./ColA -iA A.mtx -n 100 -o C.npy -cA 4 -cB 2\n"
    "    runs on 8 processes, reads A from A.mtx, replicates A 4 times,\n"
    "    generates B of size #cols_of_A rows by 100 columns,\n"
    "    and saves C to C.npy.\n"
    "\n");
    
    MPI_Finalize();
    exit(0);
  }
}

#endif  /* __COMMON_H__ */
