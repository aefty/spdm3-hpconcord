#include <algorithm>
#include <cassert>
#include <cstdio>

int main(int argc, char *argv[]) {
  assert(argc == 3);
  FILE *fa = fopen(argv[1], "rb");
  FILE *fb = fopen(argv[2], "rb");
  
  int offset = 0;
  char ba[100], bb[100];
  while (!feof(fa) && !feof(fb)) {
    int na = fread(ba, sizeof(char), 100, fa);
    int nb = fread(bb, sizeof(char), 100, fb);
    for (int i = 0; i < std::min(na, nb); ++i) {
      printf("%04d: %3d vs %3d\n", offset + i, ba[i], bb[i]);
      if (ba[i] != bb[i])
        return 0;
    }
    if (na != nb) {
      printf("%d != %d\n", na, nb);
      return 0;
    }
    offset += 100;
  }
  
  fclose(fa);
  fclose(fb);
  return 0;
}