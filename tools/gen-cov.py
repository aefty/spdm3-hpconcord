import numpy as np

x = np.random.normal(0, 1, 100000)
x = x.reshape([100, 1000])
np.save("x.npy", x.astype(np.float32, order='f'))
s = np.dot(x.transpose(), x)
s = s/100.0
np.save("s.npy", s.astype(np.float32, order='f'))
