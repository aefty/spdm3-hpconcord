#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <numeric>
#include <random>
#include <vector>

#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  if (argc < 3) {
    printf("Usage: %s <input> <output> [<seed>]\n", argv[0]);
    exit(1);
  }
  int rank, size;
  char *input = argv[1];
  char *output = argv[2];
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *blockRow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  DistDMat<IT, VT> XT(blockRow, DENSE_ROWMAJOR);
  XT.LoadNumPyTranspose(input);
  int n = XT.global_rows_;
  
  // Picks a seed.
  T.Start("Broadcasting seed");
  unsigned int seed;
  if (rank == 0) {
    seed = (argc == 4)
           ? (unsigned int) atoi(argv[3])
           : (unsigned int) time(NULL);
  }
  MPI_Bcast(&seed, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
  srand(seed);
  T.Stop("Broadcasting seed");
  
  // Generates new row mapping.
  T.Start("Generating permutation");
  std::vector<int> perm(n);
  my_iota(perm.begin(), perm.end(), 0);
  std::random_shuffle(perm.begin(), perm.end());
  T.Stop("Generating permutation");
  
  // Binning.
  // Counts how many rows will be in each processor.
  T.Start("Counting");
  int *row_counts = new int[size];
  int *send_counts = new int[size];
  int *send_displs = new int[size + 1];
  fill_vec(row_counts, size, 0);
  for (int i = XT.row_displs_[rank]; i < XT.row_displs_[rank+1]; ++i) {
    ++row_counts[find_owner_uniform_frontloaded(perm[i], n, size)];
  }
  for (int i = 0; i < size; ++i)
    send_counts[i] = row_counts[i] * XT.lcols();
  gen_displs_from_counts(size, send_counts, send_displs);
  T.Stop("Counting");
  
  // Allocates buffer.
  int buffer_size = XT.lrows() * XT.lcols();
  VT *send_buffer = new VT[buffer_size];
  VT **pointers = new VT*[size];
  for (int i = 0; i < size; ++i) {
    pointers[i] = send_buffer + send_displs[i];
  }
  
  // Prepares for Allgather.
  T.Start("Binning");
  int row_size_in_bytes = XT.lcols() * sizeof(VT);
  VT *current_row = XT.mat_.values_;
  for (int i = 0; i < XT.lrows(); ++i) {
    int owner = find_owner_uniform_frontloaded(perm[i + XT.row_displs_[rank]], n, size);
    memcpy(pointers[owner], current_row, row_size_in_bytes);
    pointers[owner] += XT.lcols();
    current_row += XT.mat_.lda_;
  }
  T.Stop("Binning");
  
  // Finds out how many rows to receive from other processes.
  T.Start("Alltoall");
  int *recv_counts = new int[size];
  int *recv_displs = new int[size + 1];
  MPI_Datatype mpi_it_ = (sizeof(IT) == 4)? MPI_INT : MPI_LONG_LONG;
  MPI_Alltoall(send_counts, 1, mpi_it_,
               recv_counts, 1, mpi_it_, MPI_COMM_WORLD);
  gen_displs_from_counts(size, recv_counts, recv_displs);
  T.Stop("Alltoall");
  
  // All-to-all.
  T.Start("Alltoallv");
  VT *recv_buffer = new VT[recv_displs[size]];
  MPI_Datatype mpi_vt_ = (sizeof(VT) == 4)? MPI_FLOAT : MPI_DOUBLE;
  MPI_Alltoallv(send_buffer, send_counts, send_displs, mpi_vt_,
                recv_buffer, recv_counts, recv_displs, mpi_vt_,
                MPI_COMM_WORLD);
  T.Stop("Alltoallv");
  
  // Now put the rows in the correct order.
  T.Start("Local shuffle");
  int recv_offset = 0;
  VT *shuffled = new VT[buffer_size];
  for (int i = 0; i < perm.size(); ++i) {
    if (find_owner_uniform_frontloaded(perm[i], n, size) == rank) {
      int row_id = perm[i] - XT.row_displs_[rank];
      int offset = row_id * XT.lcols();
      memcpy(shuffled + offset, recv_buffer + recv_offset, row_size_in_bytes);
      recv_offset += XT.lcols();
    }
  }
  assert(recv_offset == recv_displs[size]);
  T.Stop("Local shuffle");
  
  // Now writes to a separate file.
  T.Start("Write");
  char filename[100];
  sprintf(filename, "%s-%04d", output, rank);
  FILE *f_out = fopen(filename, "wb");
  unsigned int shape[2] = {(unsigned int) XT.global_cols_,
                           (unsigned int) XT.global_rows_};
  if (rank == 0)
    write_npy_header<VT>(f_out, NULL, shape, 2, true);
  fwrite(shuffled, sizeof(VT), recv_displs[size], f_out);
  fclose(f_out);
  T.Stop("Write");
  
  T.Report();
  
  // Cleans up.
  MPI_Finalize();
  delete [] row_counts;
  delete [] send_counts;
  delete [] send_displs;
  delete [] send_buffer;
  delete [] recv_counts;
  delete [] recv_displs;
  delete [] recv_buffer;
  delete [] pointers;
  
  return 0;
}
