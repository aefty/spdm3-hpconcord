#include <stdio.h>
#include "spdm3.h"
#include "utility.h"

#define IT  int
#define VT  double

using namespace spdm3;

int *dummy = NULL;
int dummy_size = 1024 * 1024 * 64;

void init_dummy() {
  dummy = new int[dummy_size];
}

// Shifts the values in the array dummy to the left by one
// and fills the last element with a new value.
void flush_cache() {
  int i;
  for (i = 0; i < dummy_size - 1; ++i)
    dummy[i] = dummy[i+1];
  dummy[i] = rand() % 1000;
}

void clear_dummy() {
  delete [] dummy;
}


int main(int argc, char *argv[]) {
  int rows = read_int(argc, argv, "-m", 5555);
  int cols = read_int(argc, argv, "-n", 3333);

  Timer T;
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  init_dummy();
  
  
  DMat<IT, VT> A(rows, cols, DENSE_ROWMAJOR);
  A.Generate();

  for (int i = 0; i < 3; ++i) {
    flush_cache();
    T.Start("Manual");
    DMat<IT, VT> B;
    B.Transpose(A);
    T.Stop("Manual");
    
    flush_cache();
    T.Start("Allocate");
    DMat<IT, VT> C(cols, rows, DENSE_ROWMAJOR);
    T.Stop("Allocate");
    T.Start("MKL");
    mkl_domatcopy('R', 'T', rows, cols, (VT) 1.0,
                  A.values_, A.lda_, C.values_, C.lda_);
    T.Stop("MKL");

    assert(B.IsEquivalent(C));
  }
  
  clear_dummy();
  T.Report();
  MPI_Finalize();
  return 0;
}