#include <stdio.h>
#include <stdlib.h>

#include "omp.h"
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

// Very simple submatrix generator.
// Matrix file format: npy, column-major
int main(int argc, char *argv[]) {

  if (argc < 3) {
    printf("Usage: %s <input> <output>\n", argv[0]);
    exit(1);
  }
  const char *input = argv[1];
  const char *output = argv[2];
  double timer, start = omp_get_wtime();
  
  DMat<IT, VT> in(DENSE_COLMAJOR);
  timer = -omp_get_wtime();
  in.LoadNumPy(input);
  timer += omp_get_wtime();
  printf("[%10.4lf] Loaded %s in %lf seconds\n",
        omp_get_wtime() - start, input, timer);
  
  DMat<IT, VT> submats[4];
  submats[0].ShallowSubDMat(in, 0, 0, 10000, 10000);
  submats[1].ShallowSubDMat(in, 0, 30000, 10000, 10000);
  submats[2].ShallowSubDMat(in, 30000, 0, 10000, 10000);
  submats[3].ShallowSubDMat(in, 30000, 30000, 10000, 10000);
  
  DMat<IT, VT> bigmat;
  timer = -omp_get_wtime();
  bigmat.Concatenate(submats, 2, 2, TWOD_ROWMAJOR);
  timer += omp_get_wtime();
  printf("[%10.4lf] Concatenated in %lf seconds\n",
        omp_get_wtime() - start, timer);
  
  timer = -omp_get_wtime();
  bigmat.SaveNumPy(output);
  timer += omp_get_wtime();
  printf("[%10.4lf] Saved %s in %lf seconds\n",
        omp_get_wtime() - start, output, timer);
  
  return 0;
}
