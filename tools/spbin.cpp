#include "spdm3.h"

#define IT int
#define VT double

using namespace spdm3;

int main(int argc, char *argv[]) {
  SpMat<IT, VT> a;
  int bin = read_int(argc, argv, "-bin", 10000);
  a.LoadMatrixMarket(argv[1]);
  printf("Done loading matrix\n");
  
  // Allocates bucket.
  int n = a.rows_;
  int dim = (n + bin - 1) / bin;
  int **bucket = new int*[dim];
  bucket[0] = new int[dim * dim];
  for (int i = 1; i < dim; ++i)
    bucket[i] = bucket[i-1] + dim;
  
  // Sets all buckets to 0.
  for (int i = 0; i < dim; ++i) {
    for (int j = 0; j < dim; ++j)
      bucket[i][j] = 0;
  }
  
  // Binning.
  for (int i = 0; i < n; ++i) {
    int bin_row_id = find_owner_uniform(i, n, dim);
    for (int k = a.headptrs_[i]; k < a.headptrs_[i+1]; ++k) {
      int col_id = a.indices_[k];
      int bin_col_id = find_owner_uniform(col_id, n, dim);
      ++bucket[bin_row_id][bin_col_id];
    }
  }
  
  // Prints the counts.
  for (int i = 0; i < dim; ++i) {
    for (int j = 0; j < dim; ++j)
      printf("%10d ", bucket[i][j]);
    printf("\n");
  }
  
  
  // Deallocation.
  delete [] bucket[0];
  delete [] bucket;
  
  return 0;
}
