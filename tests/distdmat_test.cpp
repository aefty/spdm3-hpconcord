#include "distdmat.h"

#include <cstdio>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <utility>

#include "distio.h"
#include "utility.h"

#define IT  int
#define VT  float

namespace spdm3 {

bool TestGenerateWithFunction() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistDMat<IT, VT> a(c1D);
  a.GenerateWithFunction(52, 46);  // Matrix size must be larger than num procs.
  
  Comm *c2D = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  DistDMat<IT, VT> b(c2D);
  b.GenerateWithFunction(52, 46);

  DMat<IT, VT> c = a.GetWholeMatrix();
  DMat<IT, VT> d = b.GetWholeMatrix();

  delete [] c1D;
  delete [] c2D;
  return c.IsEquivalent(d);
}

bool TestLoadNumPy() {
  const char *testfilename;
  if (sizeof(VT) == 8) {
    testfilename = "testdata/loadnumpytest.npy";
  } else if (sizeof(VT) == 4) {
    testfilename = "testdata/loadnumpytest_32bits.npy";
  } else {
    printf("ERROR! unsupported data type.\n");
    exit(1);
  }
  DistDMat<IT, VT> a = LoadNumPy<IT, VT>(testfilename);
  DMat<IT, VT> c = a.GetWholeMatrix();
  int count = 0;
  for (int j = 0; j < c.cols_; ++j) {
    for (int i = 0; i < c.rows_; ++i) {
      if (!FEQ(c.values_[i + c.lda_ * j], count))
        return false;
      ++count;
    }
  }
  return true;
}

bool TestShift1D() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistDMat<IT, VT> a(c1D);
  a.GenerateWithFunction(15, 15);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);

  bool res = true;
  int check = c1D[LAYER_COL].rank;
  for (int i = 0; i < c1D[LAYER_COL].size + 2; ++i) {
    a.Shift(LAYER_COL, 1);
    check = (check + 1) % c1D[LAYER_COL].size;
    DMat<IT, VT> b;
    b.Load("a", check);
    res &= b.IsEquivalent(a.mat_);
  }
  
  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;
  
  return res;
}

bool TestShift2D() {
  Comm *c2D = GetComms(MPI_COMM_WORLD);
  DistDMat<IT, VT> a(c2D);
  a.GenerateWithFunction(14, 18);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  bool res = true;
  int row = c2D[LAYER_COL].rank;
  int col = c2D[LAYER_ROW].rank;
  for (int i = 0; i < c2D[LAYER_COL].size; ++i) {
    a.Shift(LAYER_COL, 1);
    row = (row + 1) % c2D[LAYER_COL].size;
    DMat<IT, VT> b;
    b.Load("a", row * c2D[LAYER_ROW].size + col);
    res &= b.IsEquivalent(a.mat_);
  }
  for (int j = 0; j < c2D[LAYER_ROW].size; ++j) {
    a.Shift(LAYER_ROW, 1);
    col = (col + 1) % c2D[LAYER_ROW].size;
    DMat<IT, VT> b;
    b.Load("a", row * c2D[LAYER_ROW].size + col);
    res &= b.IsEquivalent(a.mat_);
  }
  
  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c2D[WORLD].rank);
  system(cmd);
  delete [] c2D;
  
  return res;
}

bool TestDeepCopy() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistDMat<IT, VT> a(c1D);
  a.GenerateWithFunction(25, 23);
  a.SaveLocal("a");
  
  DistDMat<IT, VT> b(c1D);
  b.DeepCopy(a);
  
  DMat<IT, VT> c;
  c.Load("a", b.rank());
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;
  return c.IsEquivalent(b.mat_);
}

bool TestBcast1D() {
  Comm *c1D = GetComms(MPI_COMM_WORLD);
  DistDMat<IT, VT> a(c1D);
  a.GenerateWithFunction(23, 21);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  DistDMat<IT, VT> b(c1D);
  b.DeepCopy(a);
  
  bool res = true;
  for (int i = 0; i < c1D[LAYER_COL].size; ++i) {
    if (c1D[LAYER_COL].rank == i)
      b.mat_.DeepCopy(a.mat_);
    b.Bcast(LAYER_COL, i);
    DMat<IT, VT> c;
    c.Load("a", i);
    res &= c.IsEquivalent(b.mat_);
  }
  
  // Cleans up.
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c1D[WORLD].rank);
  system(cmd);
  delete [] c1D;
  return res;
}

bool TestBcast2D() {
  Comm *c2D = GetComms(MPI_COMM_WORLD);
  DistDMat<IT, VT> a(c2D);
  a.GenerateWithFunction(21, 23);
  a.SaveLocal("a");
  MPI_Barrier(MPI_COMM_WORLD);
  
  DistDMat<IT, VT> b(c2D), c(c2D);
  b.DeepCopy(a);
  bool res = true;
  for (int i = 0; i < c2D[LAYER_COL].size; ++i) {
    if (c2D[LAYER_COL].rank == i)
      b.mat_.DeepCopy(a.mat_);
    b.Bcast(LAYER_COL, i);
    DMat<IT, VT> c;
    c.Load("a", i * c2D[LAYER_ROW].size + c2D[LAYER_COL].id);
    res &= c.IsEquivalent(b.mat_);
  }
  for (int j = 0; j < c2D[LAYER_ROW].size; ++j) {
    if (c2D[LAYER_ROW].rank == j)
      b.mat_.DeepCopy(a.mat_);
    b.Bcast(LAYER_ROW, j);
    DMat<IT, VT> c;
    c.Load("a", c2D[LAYER_ROW].id * c2D[LAYER_ROW].size + j);
    res &= c.IsEquivalent(b.mat_);
  }
  
  MPI_Barrier(MPI_COMM_WORLD);
  char cmd[20];
  sprintf(cmd, "rm a-%05d", c2D[WORLD].rank);
  system(cmd);
  delete [] c2D;
  return res;
}

bool TestReduce() {
  Comm *c1D = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 4);
  DistDMat<IT, VT> a(c1D);
  a.GenerateWithFunction(23, 21);
  a.mat_.Fill(1.0);
  a.Reduce(TEAM, 0);
  
  DMat<IT, VT> c;
  c.Allocate(23, 21);
  c.Fill(4.0);
  
  bool res = true;
  if (a.rank() == 0)
    res &= c.IsEquivalent(a.mat_);
  return res;
}

bool TestLoadNumPyTranspose() {
  const char *testfilename;
  if (sizeof(VT) == 8) {
    testfilename = "testdata/loadnumpytest.npy";
  } else if (sizeof(VT) == 4) {
    testfilename = "testdata/loadnumpytest_32bits.npy";
  } else {
    printf("ERROR! unsupported data type.\n");
    exit(1);
  }
  
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  DistDMat<IT, VT> XT(c1Drow, DENSE_ROWMAJOR);
  XT.LoadNumPyTranspose(testfilename);
  
  DMat<IT, VT> wxt = XT.GetWholeMatrix();
  int count = 0;
  for (int i = 0; i < wxt.rows_; ++i) {
    for (int j = 0; j < wxt.cols_; ++j) {
      if (!FEQ(wxt.values_[i * wxt.lda_ + j], count))
        return false;
      ++count;
    }
  }
  delete [] c1Drow;
  return true;
}

bool TestTranspose() {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  
  DistDMat<IT, VT> a(c1Drow);
  DistDMat<IT, VT> b(c1Dcol);
  
  a.GenerateWithFunction(29, 35);
  b.Transpose(a);
  
  DMat<IT, VT> wa = a.GetWholeMatrix();
  DMat<IT, VT> wb = b.GetWholeMatrix();
  DMat<IT, VT> wat;
  wat.Transpose(wa);

  delete [] c1Drow;
  delete [] c1Dcol;
  return wb.IsEquivalent(wat);
}

bool TestFill() {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  DistDMat<IT, VT> a(c1Drow);
  a.Fill(24, 23, 5.5);
  
  DMat<IT, VT> wa = a.GetWholeMatrix();
  DMat<IT, VT> b(24, 23);
  b.Fill(5.5);
  delete [] c1Drow;
  
  return wa.IsEquivalent(b);
}

bool TestLocalTrace() {
  int n = 123;
  Timer T;
  T.Init(MPI_COMM_WORLD);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  DistDMat<IT, VT> A(c1Dcol);
  A.GenerateWithFunction(n, n);

  VT local_trace = A.LocalTrace();
  
  DistSpMat<IT, VT> I(c1Dcol);
  I.SetIdentity(n);
  I.DotMultiplyI(A);
  VT actual_trace = I.mat_.Reduce(SPDM3_SUM);
  
  delete [] c1Dcol;
  return (local_trace == actual_trace);
}

}  // namespace spdm3

#define ADDTEST(fn, name) tests.push_back(std::make_pair(fn, name));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::pair<std::function<bool()>, std::string> > tests;

  ADDTEST(spdm3::TestGenerateWithFunction, "TestGenerateWithFunction");
  ADDTEST(spdm3::TestLoadNumPy, "TestLoadNumPy");
  ADDTEST(spdm3::TestShift1D, "TestShift1D");
  ADDTEST(spdm3::TestShift2D, "TestShift2D");
  ADDTEST(spdm3::TestDeepCopy, "TestDeepCopy");
  ADDTEST(spdm3::TestBcast1D, "TestBcast1D");
  ADDTEST(spdm3::TestBcast2D, "TestBcast2D");
  ADDTEST(spdm3::TestReduce, "TestReduce");
  ADDTEST(spdm3::TestLoadNumPyTranspose, "TestLoadNumPyTranspose");
  ADDTEST(spdm3::TestTranspose, "TestTranspose");
  ADDTEST(spdm3::TestFill, "TestFill");
  ADDTEST(spdm3::TestLocalTrace, "TestLocalTrace");
  
  MPI_Init(&argc, &argv);
  int p = 1, rank = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (p != 4) {
    std::cout << "This test must be run with 4 MPI processes. "
              << "Terminating..." << std::endl;
    exit(1);
  }
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    bool res = test.first();
    if (rank == 0) {
      MPI_Reduce(MPI_IN_PLACE, &res, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
      std::cout << std::setw(50) << test.second << ": ";
      if (res) {
        std::cout << "PASSED." << std::endl;
      } else {
        ++failed;
        std::cout << "FAILED." << std::endl;
      }
    } else {
      MPI_Reduce(&res, NULL, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
    }
  }
  
  // Summarizes.
  if (rank == 0) {
    if (!failed)
      std::cout << "All " << tests.size() << " tests passed." << std::endl;
    else
      std::cout << failed << " out of " << tests.size()
                << " tests failed." << std::endl;
  }

  MPI_Finalize();
  return 0;
}
