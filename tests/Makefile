VT   = double
ROOT = ..

include $(ROOT)/configs/config.mk

EXTLIBPATH  = $(ROOT)/external
SPBLASPATH  = $(EXTLIBPATH)/SpBLAS
SPBLASOBJS  = $(SPBLASPATH)/spblas.o

SRC     = $(ROOT)/src
INC     = $(ROOT)/include
INCLUDE = -I$(INC) -I$(SRC) -I$(EXTLIBPATH)
G500LIB = -L$(EXTLIBPATH)/graph500-1.2/generator -lgraph_generator_seq 
LIBS    = $(G500LIB) $(MATHLIB) $(SPBLASOBJS) $(OPENMP) $(MORELIBS)

CFLAGS = -std=c++11


UTIL    = $(SRC)/utility.o $(SRC)/mmio.o $(SRC)/comm.o

DEP     = $(INC)/*.h   \
          $(SRC)/*.cpp \
	  $(EXTLIBPATH)/graph500-1.2/generator/libgraph_generator_seq.a
TESTS   = dmat_test       \
          spmat_test      \
          utility_test    \
          comm_test       \
          drep_test       \
          sprep_test      \
          distmatmul_test \
          distdmat_test   \
          distspmat_test

test:	$(TESTS)
	./dmat_test
	./spmat_test
	./utility_test
	mpirun -n 32 ./comm_test
	mpirun -n 16 ./drep_test
	mpirun -n 16 ./sprep_test
	

spblas:	$(SPBLASPATH)/spblas.o


#
# SpBLAS
#
$(SPBLASPATH)/spblas.o:	$(SPBLASPATH)/nist_spblas.cc
				$(CC) -o $@ -c $< $(CFLAGS) $(INCLUDE) $(MATHLIB)

#
# Builds Graph500 generator
#
$(EXTLIBPATH)/graph500-1.2/generator/libgraph_generator_seq.a:
	$(MAKE) -C $(EXTLIBPATH)/graph500-1.2/generator


#
# Tests
# 
dmat_test:		dmat_test.cpp $(DEP) $(UTIL) spblas
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

spmat_test:		spmat_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

comm_test:		comm_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

utility_test:		utility_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)
	
drep_test:		drep_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

sprep_test:		sprep_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

distmatmul_test:	distmatmul_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

distdmat_test:		distdmat_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

distspmat_test:		distspmat_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

dummy_test:		dummy_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

shift_test:		shift_test.cpp $(DEP) spblas $(UTIL)
			$(MPCC) -o $@ $< $(CFLAGS) $(LIBS) $(INCLUDE) $(UTIL)

#
# General rule for .o
#
$(SRC)/%.o:    $(SRC)/%.cpp $(INC)/%.h 
		$(MPCC) -o $@ -c $< $(OPENMP) $(CFLAGS) $(INCLUDE) $(MATHLIB) -DTEST
$(SRC)/%.o:    $(SRC)/%.c $(INC)/%.h 
		$(MPCC) -o $@ -c $< $(OPENMP) $(CFLAGS) $(INCLUDE) $(MATHLIB) -DTEST

clean:
		rm -f *.o $(TESTS)

cleanall:
		rm -f $(TESTS) $(SRC)/*.o
		rm -f $(EXTLIBPATH)/graph500-1.2/generator/*.o
		rm -f $(EXTLIBPATH)/graph500-1.2/generator/libgraph_generator_seq.a
		rm -f $(SPBLASOBJS)
