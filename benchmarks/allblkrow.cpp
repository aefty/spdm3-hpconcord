#include <stdio.h>
#include "spdm3.h"
#include "flush.h"

#define IT  int
#define VT  double

using namespace spdm3;

int main(int argc, char *argv[]) {
  int n = read_int(argc, argv, "-n", 20000);
  int ca = read_int(argc, argv, "-ca", 1);
  int cb = read_int(argc, argv, "-cb", 1);
  int tests = read_int(argc, argv, "-t", 3);
  
  double percent_r = read_double(argc, argv, "-pr", 5.0);
  double percent_nnz = read_double(argc, argv, "-pnnz", 5.0);
  int r = n * percent_r / 100.0;
  int d = n * percent_nnz / 100.0;
  Timer T;
  
  // Initializations.
  MPI_Init(&argc, &argv);
  T.Init(MPI_COMM_WORLD);
  init_dummy();
  
  Comm *commO  = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, ca);
  Comm *commXT = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, cb);
  
  DistDMat<IT, VT> O(commO);
  DistDMat<IT, VT> XT(commXT);
  DistDMat<IT, VT> Y(commO);
  
  O.GenerateWithFunction(d, n);
  XT.GenerateWithFunction(n, r);
  
  for (int test = 0; test < tests; ++test) {
    flush_cache();
    T.print("round %d\n", test);
    fflush(stdout);
    MPI_Barrier(MPI_COMM_WORLD);
    allblkrow_shiftc(O, XT, Y, T);
  }
  
  T.print("End\n");
  fflush(stdout);
  MPI_Barrier(MPI_COMM_WORLD);
  
  // Finalizations.
  clear_dummy();
  T.printr("%s: n = %d, percent_r = %f, percent_nnz = %f, "
           "p = %d, ca = %d, cb = %d, tests = %d, input = (null), nnz = %d\n",
           argv[0], n, percent_r, percent_nnz,
           O.nprocs(), c, c, tests, d * n);
  T.Report();
  MPI_Finalize();
  return 0;
}